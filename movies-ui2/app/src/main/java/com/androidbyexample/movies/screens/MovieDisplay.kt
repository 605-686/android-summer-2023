package com.androidbyexample.movies.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.R
import com.androidbyexample.movies.Screen
import com.androidbyexample.movies.components.MovieScaffold
import com.androidbyexample.movies.components.SimpleText
import com.androidbyexample.movies.repository.MovieWithCastDto

@Composable
fun MovieDisplay(
    movieWithCastDto: MovieWithCastDto?,

// BEGIN REMOVE FOR UPDATE
//    movieId: String,
//    fetchMovieWithCast: suspend (String) -> MovieWithCastDto,
// END REMOVE FOR UPDATE
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onActorClick: (String) -> Unit,
    onEdit: (String) -> Unit,
) {
// BEGIN REMOVE FOR UPDATE
//    var movieWithCastDto by remember { mutableStateOf<MovieWithCastDto?>(null) }
//
//    LaunchedEffect(key1 = movieId) {
//        // starts a coroutine to fetch the rating
//        movieWithCastDto = fetchMovieWithCast(movieId)
//    }
// END REMOVE FOR UPDATE

    MovieScaffold(
        title = movieWithCastDto?.movie?.title ?: stringResource(id = R.string.loading),
        onSelectListScreen = onSelectListScreen,
        onResetDatabase = onResetDatabase,
        onEdit =
            movieWithCastDto?.let { movieWithCast ->
                {
                    onEdit(movieWithCast.movie.id)
                }
            }
    ) { paddingValues ->
// BEGIN REMOVE FOR UPDATE
//        val movieDto = movieWithCastDto // capture to enable smart cast
// clean up refs below
// END REMOVE FOR UPDATE

        if (movieWithCastDto == null || movieWithCastDto.cast.isEmpty()) {
            SimpleText(
                text = stringResource(id = R.string.no_actors_found_for_this_movie),
                modifier = Modifier.padding(paddingValues)
            )
        } else {
            Column(modifier = Modifier.padding(paddingValues)) {
                SimpleText(text = stringResource(id = R.string.cast, movieWithCastDto.movie.title))

                LazyColumn(
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .weight(1f)
                ) {
                    items(
                        items = movieWithCastDto.cast
                    ) { roleWithActor ->
                        Card(
                            elevation = CardDefaults.cardElevation(),
                            modifier = Modifier
                                .padding(8.dp)
                                .fillMaxWidth()
                        ) {
                            Row(
                                modifier = Modifier
                                    .fillMaxWidth()
                                        .clickable {
                                            onActorClick(roleWithActor.actor.id)
                                        }
                            ) {
                                SimpleText(
                                    text = roleWithActor.character,
                                    modifier = Modifier.weight(42f)
                                )
                                SimpleText(
                                    text = roleWithActor.actor.name,
                                    modifier = Modifier
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}
