package com.androidbyexample.movies.screens

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.androidbyexample.movies.R
import com.androidbyexample.movies.Screen
import com.androidbyexample.movies.components.ListScaffold
import com.androidbyexample.movies.components.SimpleText
import com.androidbyexample.movies.repository.ActorDto

@Composable
fun ActorList(
    actors: List<ActorDto>,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    selectedItemIds: Set<String>,
    onActorClick: (String) -> Unit,
    onClearSelections: () -> Unit,
    onToggleSelection: (String) -> Unit,
    onDeleteSelectedItems: () -> Unit,
) = ListScaffold(
    title = stringResource(id = R.string.screen_title_actors),
    onSelectListScreen = onSelectListScreen,
    onResetDatabase = onResetDatabase,
    items = actors,
    icon = Icons.Default.Person,
    iconDescriptionId = R.string.tap_to_toggle_selection,
    onItemClick = onActorClick,
    selectedItemIds = selectedItemIds,
    onClearSelections = onClearSelections,
    onToggleSelection = onToggleSelection,
    onDeleteSelectedItems = onDeleteSelectedItems,
) {
    SimpleText(text = it.name)
}
