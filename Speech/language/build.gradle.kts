plugins {
    id("java-library")
    id("kotlin")
    id("antlr")
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

dependencies {
    antlr("org.antlr:antlr4:4.12.0")
}

tasks.named("compileKotlin") {
    dependsOn("generateGrammarSource")
}

tasks.generateGrammarSource {
    arguments = arguments + listOf("-package", "com.javadude.speech.language")
}