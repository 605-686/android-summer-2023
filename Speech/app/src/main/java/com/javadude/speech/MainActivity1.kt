@file:OptIn(ExperimentalMaterial3Api::class)

package com.javadude.speech

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.Key
import androidx.compose.ui.input.key.key
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import com.javadude.speech.language.Game
import com.javadude.speech.ui.theme.SpeechTheme

@ExperimentalComposeUiApi
class MainActivity1 : ComponentActivity() {
    private lateinit var game: Game
    private var text by mutableStateOf("")
    private var status by mutableStateOf("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        game = Game(
            onQuit = { finish() },
            reporter = { message ->
                text = "${game.getLocationInfo()}\n\n$message"
            }
        )
        game.look()

        setContent {
            SpeechTheme {
                Surface(color = MaterialTheme.colorScheme.background) {
                    Ui(status, text,
                        onCommand = { command ->
                            game.parse(command)
                        },
                    )
                }
            }
        }
    }

}

@ExperimentalComposeUiApi
@Composable
fun Ui(
    status: String,
    text: String,
    onCommand: (String) -> Unit
) {
    var command by remember { mutableStateOf("") }
    Scaffold(
        topBar = { TopAppBar(title = { Text("Adventure!") }) },
        content = { paddingValues ->
            Column(modifier = Modifier.padding(paddingValues).padding(8.dp)) {
                OutlinedTextField(
                    label = { Text(stringResource(id = R.string.enter_command)) },
                    value = command,
                    singleLine = true,
                    textStyle = MaterialTheme.typography.titleLarge,
                    keyboardActions = KeyboardActions {
                        onCommand(command)
                        command = ""
                    },
                    keyboardOptions = KeyboardOptions(
                        imeAction = ImeAction.Go,
                    ),
                    onValueChange = { command = it },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(8.dp)
                        // as of Compose v1.1, the following onKeyEvent() call is no longer needed
                        .onKeyEvent {
                            if (it.key == Key.Enter) {
                                onCommand(command.trim())
                                command = ""
                                true
                            } else {
                                false
                            }
                        }
                )
                MyText(text = status)
                MyText(text = text)
            }
        }
    )
}
