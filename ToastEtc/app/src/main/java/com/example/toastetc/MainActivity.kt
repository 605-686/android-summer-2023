@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.toastetc

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.example.toastetc.ui.theme.ToastEtcTheme
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ToastEtcTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    var snackbarHostState = remember { SnackbarHostState() }
                    var idsToDelete by remember { mutableStateOf(setOf("m1", "m2")) }
                    var showDeletionConfirmation by remember { mutableStateOf(false) }
                    val context = LocalContext.current // Composition local
                    val scope = rememberCoroutineScope()

                    if (showDeletionConfirmation) {
                        AlertDialog(
                            title = { Text("Delete") },
                            text = { Text("Delete ${idsToDelete.size} movies?") },
                            onDismissRequest = { }, // click outside
                                // do nothing - too easy to do accidentally
                            confirmButton = {
                                MyButton(text = "Ok") {
                                    // if we want undo, save the movies that are being deleted
                                    // do the actual deletion

                                    // report deletion success
//                                    showToast(
//                                        context,
//                                        "${idsToDelete.size} movies deleted")

                                    scope.launch {
                                        val result = snackbarHostState.showSnackbar(
                                            message = "${idsToDelete.size} movies deleted",
                                            actionLabel = "Undo",
                                            withDismissAction = true,
                                            duration = SnackbarDuration.Long
                                        )
                                        when (result) {
                                            SnackbarResult.Dismissed -> { } // do nothing
                                            SnackbarResult.ActionPerformed -> {
                                                // re-add the deleted movies
                                                showToast(
                                                    context,
                                                    "${idsToDelete.size} movies restored")
                                            }
                                        }
                                    }

                                    showDeletionConfirmation = false
                                }
                            },
                            dismissButton = {
                                MyButton(text = "Cancel") {
                                    showDeletionConfirmation = false
                                }
                            },
                        )
                    }

                    Scaffold(
                        snackbarHost = { SnackbarHost(snackbarHostState) },
                        topBar = {
                            TopAppBar(
                                title = { Text("Toast, etc.") },
                                actions = {
                                    MyIconButton(icon = Icons.Filled.Delete, text = "Delete") {
                                        showDeletionConfirmation = true
                                    }
                                }
                            )
                        }
                    ) { paddingValues ->
                        Box(
                            modifier = Modifier
                                .padding(paddingValues)
                                .fillMaxSize()
                        ) {
                            // normally this would be in the view model

                            // would normally be lazycolumn of movies
                            Column {
                                NormalText(text = "Hobbs and Shaw")
                                SelectedText(text = "Jumanji: Welcome to the Jungle")
                                SelectedText(text = "The Transporter")
                                NormalText(text = "Transporter 2")
                            }
                        }
                    }
                }
            }
        }
    }

    private var lastToast: Toast? = null

    fun showToast(context: Context, message: String) {
        lastToast?.cancel()
        lastToast = Toast.makeText(
            context,
            message,
            Toast.LENGTH_LONG
        ).apply { show() }
    }

}

@Composable
fun NormalText(
    text: String
) =
    Card(modifier = Modifier
        .padding(8.dp)
        .fillMaxWidth()) {
        Text(
            text = text,
            modifier = Modifier.padding(8.dp)
        )
    }

@Composable
fun SelectedText(
    text: String
) =
    Card(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primary,
            contentColor = MaterialTheme.colorScheme.onPrimary,
        ),
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
    ) {
        Text(
            text = text,
            modifier = Modifier.padding(8.dp)
        )
    }


@Composable
fun MyButton(
    text: String,
    onClick: () -> Unit,
) {
    Button(onClick = onClick) {
        Text(text = text, modifier = Modifier.padding(8.dp))
    }
}

@Composable
fun MyIconButton(
    icon: ImageVector,
    text: String,
    onClick: () -> Unit,
) {
    IconButton(onClick = onClick) {
        Icon(imageVector = icon, contentDescription = text, modifier = Modifier.padding(8.dp))
    }
}