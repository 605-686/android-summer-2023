@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.toastetc

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.example.toastetc.ui.theme.ToastEtcTheme
import kotlinx.coroutines.launch

class MainActivity2 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ToastEtcTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val snackbarHostState = remember { SnackbarHostState() }
                    val dialogHostState = remember { DialogHostState() }
                    var idsToDelete by remember { mutableStateOf(setOf("m1", "m2")) }
                    val context = LocalContext.current // Composition local
                    val scope = rememberCoroutineScope()

                    ScaffoldWithDialog(
                        dialogHost = { DialogHost(dialogHostState)},
                        snackbarHost = { SnackbarHost(snackbarHostState) },
                        topBar = {
                            TopAppBar(
                                title = { Text("Toast, etc.") },
                                actions = {
                                    MyIconButton(icon = Icons.Filled.Delete, text = "Delete") {
                                        dialogHostState.showDialog(
                                            title = "Delete",
                                            message = "Delete ${idsToDelete.size} movies?",
                                            onOk = {
                                                // if we want undo, save the movies that are being deleted
                                                // do the actual deletion

                                                // report deletion success
                                                scope.launch {
                                                    val result = snackbarHostState.showSnackbar(
                                                        message = "${idsToDelete.size} movies deleted",
                                                        actionLabel = "Undo",
                                                        withDismissAction = true,
                                                        duration = SnackbarDuration.Long
                                                    )
                                                    when (result) {
                                                        SnackbarResult.Dismissed -> { } // do nothing
                                                        SnackbarResult.ActionPerformed -> {
                                                            // re-add the deleted movies
                                                            showToast(
                                                                context,
                                                                "${idsToDelete.size} movies restored")
                                                        }
                                                    }
                                                }

                                            },
                                            onCancel = {},
                                        )
                                    }
                                }
                            )
                        }
                    ) { modifier ->
                        Box(
                            modifier = modifier.fillMaxSize()
                        ) {
                            // normally this would be in the view model

                            // would normally be lazycolumn of movies
                            Column {
                                NormalText(text = "Hobbs and Shaw")
                                SelectedText(text = "Jumanji: Welcome to the Jungle")
                                SelectedText(text = "The Transporter")
                                NormalText(text = "Transporter 2")
                            }
                        }
                    }
                }
            }
        }
    }

    private var lastToast: Toast? = null

    fun showToast(context: Context, message: String) {
        lastToast?.cancel()
        lastToast = Toast.makeText(
            context,
            message,
            Toast.LENGTH_LONG
        ).apply { show() }
    }
}

class DialogData(
    val title: String,
    val message: String,
    val onOk: () -> Unit,
    val onCancel: (() -> Unit)?,
)

class DialogHostState {
    var currentDialogData by mutableStateOf<DialogData?>(null)
        private set

    fun showDialog(
        title: String,
        message: String,
        onOk: () -> Unit,
        onCancel: (() -> Unit)? = null,
    ) {
        currentDialogData = DialogData(title, message, onOk, onCancel)
    }

    fun dismissDialog() {
        currentDialogData = null
    }
}

@Composable
fun DialogHost(
    hostState: DialogHostState,
) {
    hostState.currentDialogData?.let { data ->
        AlertDialog(
            title = { Text(data.title) },
            text = { Text(data.message) },
            onDismissRequest = { }, // click outside
            // do nothing - too easy to do accidentally
            confirmButton = {
                MyButton(text = "Ok") {
                    data.onOk()
                    hostState.dismissDialog()
                }
            },
            dismissButton = {
                data.onCancel?.let { onCancel ->
                    MyButton(text = "Cancel") {
                        onCancel()
//                        data.onCancel?.invoke()
                        hostState.dismissDialog()
                    }
                }
            },
        )
    }
}

@Composable
fun ScaffoldWithDialog(
    modifier: Modifier = Modifier,
    topBar: @Composable () -> Unit = {},
    snackbarHost: @Composable () -> Unit = {},
    dialogHost: @Composable () -> Unit = {},
    content: @Composable (Modifier) -> Unit
) {
    Scaffold(
        modifier = modifier,
        snackbarHost = snackbarHost,
        topBar = topBar,
    ) {  paddingValues ->
        Box(modifier = Modifier
            .padding(paddingValues)
            .padding(8.dp)) {

            dialogHost()

            content(Modifier.fillMaxSize())
        }
    }
}