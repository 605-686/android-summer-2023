package com.example.files

import java.io.FilterWriter
import java.io.Writer

class UppercaseWriter(out: Writer) : FilterWriter(out) { // DECORATOR PATTERN
    override fun write(c: Int) {
        super.write(c.toChar().uppercase())
    }
    override fun write(cbuf: CharArray, off: Int, len: Int) {
        write(cbuf.toString(), off, len)
    }
    override fun write(str: String, off: Int, len: Int) {
        super.write(str.uppercase(), off, len)
    }
}