package com.androidbyexample.movies

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.androidbyexample.movies.repository.MovieDatabaseRepository
import com.androidbyexample.movies.repository.MovieRepository
import kotlinx.coroutines.launch

sealed interface Screen
object RatingListScreen: Screen
object MovieListScreen: Screen
object ActorListScreen: Screen
data class RatingDisplayScreen(
    val id: String
): Screen
data class ActorDisplayScreen(
    val id: String
): Screen
data class MovieDisplayScreen(
    val id: String
): Screen

class MovieViewModel(
    private val repository: MovieRepository
) : ViewModel() {
    var screen by mutableStateOf<Screen?>(MovieListScreen)
        private set

    private var screenStack: List<Screen> = listOf(MovieListScreen)
        set(value) {
            field = value
            screen = value.lastOrNull()
        }

    fun pushScreen(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun popScreen() {
        if (screenStack.isNotEmpty()) {
            screenStack = screenStack.dropLast(1)
        }
    }
    fun setScreenStack(screen: Screen) {
        screenStack = listOf(screen)
    }

    val ratingsFlow = repository.ratingsFlow
    val moviesFlow = repository.moviesFlow
    val actorsFlow = repository.actorsFlow

    suspend fun getRatingWithMovies(id: String) =
        repository.getRatingWithMovies(id)

    suspend fun getMovieWithCast(id: String) =
        repository.getMovieWithCast(id)

    suspend fun getActorWithFilmography(id: String) =
        repository.getActorWithFilmography(id)

    fun resetDatabase() {
        viewModelScope.launch {
            repository.resetDatabase()
        }
    }

    companion object {
        val Factory: ViewModelProvider.Factory = object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(
                modelClass: Class<T>,
                extras: CreationExtras
            ): T {
                // Get the Application object from extras
                val application = checkNotNull(extras[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY])
                return MovieViewModel(
                    MovieDatabaseRepository.create(application)
                ) as T
            }
        }
    }
}