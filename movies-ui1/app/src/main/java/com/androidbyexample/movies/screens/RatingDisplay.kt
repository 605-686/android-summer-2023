package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.androidbyexample.movies.R
import com.androidbyexample.movies.Screen
import com.androidbyexample.movies.components.MovieScaffold
import com.androidbyexample.movies.components.SimpleText
import com.androidbyexample.movies.repository.RatingWithMoviesDto

@Composable
fun RatingDisplay(
    ratingId: String,
    fetchRatingWithMovies: suspend (String) -> RatingWithMoviesDto,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onMovieClick: (String) -> Unit,
) {
    var ratingWithMoviesDto by remember { mutableStateOf<RatingWithMoviesDto?>(null) }

    LaunchedEffect(key1 = ratingId) {
        // starts a coroutine to fetch the rating
        ratingWithMoviesDto = fetchRatingWithMovies(ratingId)
    }

    MovieScaffold(
        title = ratingWithMoviesDto?.rating?.name ?: stringResource(id = R.string.loading),
        onSelectListScreen = onSelectListScreen,
        onResetDatabase = onResetDatabase,
    ) { paddingValues ->
        Column(modifier = Modifier.padding(paddingValues)) {
            ratingWithMoviesDto?.let { ratingWithMovies ->
                ratingWithMovies.movies.forEach { movie ->
                    SimpleText(text = movie.title) {
                        onMovieClick(movie.id)
                    }
                }
            }
        }
    }
}