plugins {
    kotlin("jvm") version "1.8.21"
    application
}

group = "com.androidbyexample"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}

application {
//    mainClass.set("sample1.MainKt")
    mainClass.set("stuff.Test1")
}