package sample1

open class Sample1( // primary constructor
    x: Int,
    y:Int
) {
    val x: Int = x // cannot set - kind of like IMMUTABLE
    val y: Int = y // cannot set - kind of like IMMUTABLE

    val sum: Int
        get() {
            return x + y // single-expression function
        }
    val sum2: Int
        get() = x + y // single-expression value function

    // example of "mutable" val
    private var zValue: Int = 42
    val z: Int
        get() = zValue // kinda evil, eh?
}

open class Sample2(
    val x: Int,
    val y: Int
)

class SubSample1: Sample1(10, 20)
class SubSample2: Sample2(10, 20)
class SubSample2a(x: Int, y: Int): Sample2(x, y)


open class Point(
    val x: Int,
    val y: Int
)

class Point3D(
    x: Int,
    y: Int,
    val z: Int,
): Point(x, y)

fun main() {
    val sample1 = Sample1(10, 20)
    println(sample1.sum)
}