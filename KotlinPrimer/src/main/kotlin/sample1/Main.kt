package sample1

// runs on JVM, JS, Native, Android, iOS, anywhere Java runs

// Object-Oriented/Functional
//      pure functions
//          parameters immutable
//          no side effects
//          idempotent (same input -> same output)


fun main() {
    println("Hello World!")

}