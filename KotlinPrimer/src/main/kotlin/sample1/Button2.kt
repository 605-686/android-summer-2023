package sample1

import stuff.OnClickListener

fun interface OnClickListener2 { // explicitly defining functional interface
    fun onClick(button2: Button2) // SAM (single abstract method)
}

class Button2 {
    var onClickListener2: OnClickListener2? = null

    private fun detectClickGesture() {
        // called when the user presses and releases on the button
//        if (onClickListener2 != null) {
//            // what if someone changes onClickListener2 here???
//            onClickListener2?.onClick(this)
//        }
//        val staticOnClickListener2 = onClickListener2 // capture the current value
//        if (staticOnClickListener2 != null) {
//            staticOnClickListener2.onClick(this)
//        }
        // can replace with safe access
        val staticOnClickListener2 = onClickListener2 // capture the current value
        staticOnClickListener2?.onClick(this)

        // better
        onClickListener2?.onClick(this)
        // might change
        onClickListener2?.onClick(this)
        // might change
        onClickListener2?.onClick(this)
        // might change
        onClickListener2?.onClick(this)

        val staticOnClickListener2a = onClickListener2 // capture the current value
        staticOnClickListener2a?.onClick(this)
        staticOnClickListener2a?.onClick(this)
        staticOnClickListener2a?.onClick(this)
        staticOnClickListener2a?.onClick(this)

        // scoping function
        val result = onClickListener2?.let { // scoping function; higher-order function
            it.onClick(this)
            it.onClick(this)
            it.onClick(this)
            it.onClick(this)
            42
        }

        onClickListener2?.myLet {  // if single parameter and not named, "it" is define
        }
        onClickListener2?.myLet { listener -> // no more "it"
            listener.onClick(this)
            listener.onClick(this)
            listener.onClick(this)
        }
        onClickListener2?.myLet { _ -> // if param not needed, use _
            println(",,,,")
        }
    }
}

// higher order function - takes function(s) as parameter(s)
// extension function - defined on top of OnClickListener2
fun OnClickListener2.myLet(block: (OnClickListener2) -> Unit) {
    block(this)
}


// extension function
//   making it look like you're adding a function to a type

class Pointy(
    val x: Int,
    val y: Int,
)
class Triangle(
    val p1: Pointy,
    val p2: Pointy,
    val p3: Pointy,
)

// extension function on Triangle
fun Triangle.center(): Pointy {
    // compute center and return
    return Pointy(10, 20)
}