package sample1

object DummyUI2 {
    class MyOnClickListener2: OnClickListener2 {
        override fun onClick(button2: Button2) {
            println("$button2 clicked")
            // TODO("Not yet implemented") // TODO function throws exception - useful!!!
        }
    }
    @JvmStatic
    fun main(args: Array<String>) {
        val button2 = Button2()
        val myOnClickListener2 = MyOnClickListener2()
        button2.onClickListener2 = myOnClickListener2

        button2.onClickListener2 = object: OnClickListener2 { // equiv of anon inner class
            override fun onClick(button2: Button2) {
                println("$button2 clicked")
            }
        }

        button2.onClickListener2 = OnClickListener2 { button2: Button2 ->
            println("$button2 clicked")
        }
    }
}