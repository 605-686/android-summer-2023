package sample1

fun main() {
    val person = Person()
    println(person)
    person.name = "Scott" // calls set()
    person.age = 56 // calls set()
    println(person)
}

class Person {
    var name: String = "" // VAR - has setter - can be requested to be changed ("MUTABLE")
    var age: Int = 0



    // other ways to set up properties
    var nameX: String = ""
        get() { // implicit getter look like this
            return field // backing field
        }
        set(value) {  // implicit setter looks like this
            field = value
        }

    // properties without backing fields are typically called "derived properties"
    var nameY: String // has no backing field - no initializer needed/allowed
        get() { // implicit getter look like this
            return "foo" // NO backing field mentioned
        }
        set(value) {  // implicit setter looks like this
            // do nothing - no backing field mentioned
        }
    var ageX: Int = 0
        get() { // implicit getter look like this
            return field
        }
        set(value) {  // implicit setter looks like this
            field = value
        }

    override fun toString(): String {
        return "Person(name='$name', age=$age)"
    }
}
