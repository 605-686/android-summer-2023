package sample1

class UI {
    var data1: Int = 10
    var data2: Int = 10
    var data3: Int = 10
}

class ViewModel {
    private val ui = UI().apply {  // initialize
        data1 = 20
    }
}

object Scoping {
    @JvmStatic
    fun main(args: Array<String>) {

    }

    fun foo(x: Int?) {

        // typically let is used for "if not null" capture and use the value
        // receiver passed as "it" or single named arg - result is last expresssion
        val result = x?.let {
            println(it)
            val y = x + 10
            println(y)
        }

        // receiver passed as "this" - result is last expresssion
        val result2 = x?.run {
            42
        }

        // typically used for initialization
        // receiver passed as "this" - result is receiver
        val result3 = x?.apply {
        }

        // receiver passed as "it" - result is receiver
        val result4 = x?.also {
        }

        val ui = UI()
        with(ui) {
            data1 = 1
            data2 = 2
            data3 = 3
        }

        // other scoping functions
        //   apply - typically for initialization
        //   run - do something with a value - try not to use
        //   also - do something with a value - try not to use
        //   with - do something with a value - try not to use
        // what goes in, what comes out

        var x = 10
        var y = 20

        x = y.also { y = x } // swap values - DON'T DO THIS - BUT YOU'LL SEE IT...
        // pass y as "it" into also (and ignore)
        // body of also is called
        //   assigns x to y
        // return y at end of also
        // assign original y to x

        // really - do this
        val temp = x
        x = y
        y = temp

    }
}