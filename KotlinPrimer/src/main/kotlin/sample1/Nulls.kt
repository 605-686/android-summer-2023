package sample1

data class NonNullablePerson(
    val name: String, // non nullable
    val age: Int,
)
data class NullablePerson(
    val name: String?, // nullable
    val age: Int,
)

fun main() {
//    val nonNullablePerson = NonNullablePerson(null, 56) // BAD
    val nonNullablePerson = NonNullablePerson("Scott", 56) // OK
    val nullablePerson = NullablePerson(null, 56) // OK

    val nonNullableNameLength = nonNullablePerson.name.length

//    val nullableNameLength = nullablePerson.name!!.length // non-null assertion
    // in this case, it will fail
    //    "trust me, I know what I'm doing"
    // try not to use this even if you're pretty sure

    val safeNullableNameLength = nullablePerson.name?.length // will return null
        // type Int?
    val safeNullableNameLengthWithDefault = nullablePerson.name?.length ?: 0 // Elvis
        // type Int
    println(safeNullableNameLength)
    println(safeNullableNameLengthWithDefault)

    val safeNullableNameLengthWithException =
        nullablePerson.name?.length ?: throw RuntimeException("oops!")
        // type Int

    // ALL EXCEPTIONS IN KOTLIN ARE NON-CHECKED
}