package sample1

fun foo(a: Int) {
    if (a < 10) {
        println("low")
    } else {
        println("high")
    }

    when (a) { // NO FALL THROUGH
        1 -> println("very low")
        2 -> println("low")
        3 -> {
            println("medium")
            println("medium")
            println("medium")
            println("medium")
        }
        4 -> println("high")
        5 -> println("very high")
        else -> println("????")
    }
}

fun getMessage(code: Int): String {
    when (code) { // THIS IS BAD HTTP CODE HANDLING FOR EXAMPLE ONLY
        200 -> return "Success"
        404 -> return "Not Found"
        else -> return "Error"
    }
}
fun getMessage2(code: Int): String { // WHEN is an expression!
    return when (code) { // THIS IS BAD HTTP CODE HANDLING FOR EXAMPLE ONLY
        200 -> "Success"
        404 -> "Not Found"
        else -> "Error"
    }
}
fun getMessage3(code: Int) = // single-expression function
     when (code) { // THIS IS BAD HTTP CODE HANDLING FOR EXAMPLE ONLY
        200 -> "Success"
        404 -> "Not Found"
        else -> "Error"
    }

// CAN USE IF AS AN EXPRESSION AS WELL!!!

fun ifExpression(value: Int) =
    if (value < 100) 42 else 15 // similar to ternary expression in Java

fun main() {
    foo(3)
    println(getMessage(404))

    val a = 10

    val x = ifExpression(10)
    println(x)
}