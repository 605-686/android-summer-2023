package sample1

data class Dog(
    val name: String,
    val age: Int,
    val numberOfBonesEaten: Int = 0,
) {
//    fun copy(
//        name: String = this.name,
//        age: Int = this.age,
//        numberOfBonesEaten: Int = this.numberOfBonesEaten,
//    ): Dog {
//        return Dog(name, age, numberOfBonesEaten)
//    }
}

fun main() {
    val dog1 = Dog("Fido", 3, 1423)
    val dog1a = Dog(
        name = "Fido",
        age = 3,
        numberOfBonesEaten = 1423
    )
    val dog1b = Dog(
        name = "Fido",
        age = 3,
    )
}