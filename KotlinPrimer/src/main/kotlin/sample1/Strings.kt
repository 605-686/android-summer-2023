package sample1

data class Person2(
    val name: String,
    val age: Int,
)

fun mainX() {
    val x1 = 42
    val x2 = "Hello"
    val x3 = Person2("Scott", 56)

    val message = "x1 is $x1, x2 is $x2 and x3's name is ${x3.name}"

    val lines = // raw string
        """
            line1
            line2
                line3
            line4
        """.trimIndent()

    val lines2 = // raw string
        """
            |line1
            |line2
            |    line3
            |line4
        """.trimMargin()

    println(message)
    println("------------------")
    println(lines)
    println("------------------")
    println(lines2)
    println("------------------")
}