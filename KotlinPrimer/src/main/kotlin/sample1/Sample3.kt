package sample1

data class Sample3(
    val x: Int, // component1()
    val y: Int // component2()
)

class Sample3X {
//    val x: Int = 10
//    val y: Int = 20
    val x = 10
    val y = 20

    val x2: Long = 10
    val y2 = 20L

    override fun toString(): String {
        return "Sample3a(x=$x, y=$y)"
    }
}

// data classes get
//    toString()
//    hashCode()
//    equals()
//    copy()
//    component1()
//    component2()
// all based on properties defined in primary constructor

fun main() {
    val sample3a = Sample3(10, 20)
    val sample3b = Sample3(10, 20)
    println(sample3a)

    println(sample3a == sample3b)
    println(sample3a === sample3b)

    val sample3x = Sample3X()
    println(sample3x)

    val (x, y) = sample3a // deconstruction declaration

    // using copy from data class
    val newSample3a1 = sample3a.copy() // all values the same
    val newSample3a2 = sample3a.copy(20, 30) // changing all values
    val newSample3a3 = sample3a.copy(x = 20, y = 30) // changing all values with explicit names
    val newSample3a4 = sample3a.copy(y = 20, x = 30) // changing all values with explicit names
    val newSample3a5 = sample3a.copy(x = 20) // changing just x; same value for y
}
