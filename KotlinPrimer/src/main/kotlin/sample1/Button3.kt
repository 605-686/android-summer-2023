package sample1

class Button3 {
//    var onClickListener2: ((Button3) -> Unit)? = null
    var onClickListener2: (Button3) -> Unit = { } // "null object" pattern

    private fun detectClickGesture() {
        // called when the user presses and releases on the button
        if (onClickListener2 != null) {
//            onClickListener2.onClick(this)
        }
    }
}