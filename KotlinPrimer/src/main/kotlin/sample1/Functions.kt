package sample1

class Functions {
    fun foo1() { // implicit return type of "Unit"
        println("Hello")
    }
    fun foo2(): Unit { // explicit return type of "Unit"
        println("Hello")
    }
    fun getThingToPrint(): String {
        return "Bye"
    }

    fun add(x: Int, y: Int): Int {
        return x + y
    }
    fun add2(x: Int, y: Int) = x + y // single-expression function
}

// BTW on semicolons
enum class Suits(val value: Int) {
    Spades(4), Hearts(3), Diamonds(2), Clubs(1); // only place you NEED ;

    fun value(): Int {
        if (this == Spades) return 4
        if (this == Hearts) return 3
        if (this == Diamonds) return 2
        return 1
    }
}

fun main() {
    val functions = Functions()
    functions.foo1()
    functions.foo2()
    println(functions.getThingToPrint())
}