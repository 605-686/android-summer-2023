package stuff;

public class DummyUI {

    private static class MyOnClickListener implements OnClickListener {
        @Override
        public void onClick(Button button) {
            System.out.println("Clicked on button " + button);
        }
    }

    public static void main(String[] args) {
        Button button = new Button();
        MyOnClickListener myOnClickListener = new MyOnClickListener();
        button.setOnClickListener(myOnClickListener);
            // when button is pressed, onClick is called

        button.setOnClickListener(new OnClickListener() { // anonymous inner class
            @Override
            public void onClick(Button button) {
                System.out.println("Clicked on button " + button);
            }});

        // lambdas
        button.setOnClickListener(button1 -> System.out.println("Clicked on button " + button1));
        button.setOnClickListener(button1 -> {
            // do other stuff
            System.out.println("Clicked on button " + button1);
        });

        // lambdas LOOK like closures, but are not!

        int x = 42;
        button.setOnClickListener(button1 -> {
            System.out.println(x); // "effectively final" - cannot change it after!
            // x = 43; // nope! it's effectively final!
            System.out.println("Clicked on button " + button1);
        });
    }
}
