package stuff;

public class Button { // Observable
    private OnClickListener onClickListener;

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    private void detectClickGesture() {
        // called when the user presses and releases on the button
        if (onClickListener != null) {
            onClickListener.onClick(this);
        }
    }
}
