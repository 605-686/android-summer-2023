package stuff;

// Observer Pattern for a Button

public interface OnClickListener { // Observer
    void onClick(Button button);
}
