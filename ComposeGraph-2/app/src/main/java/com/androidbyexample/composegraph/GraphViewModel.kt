package com.androidbyexample.composegraph

import android.util.Log
import androidx.compose.ui.geometry.Offset
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

class GraphViewModel: ViewModel() {
    private val _highlightShapeType = MutableStateFlow<ShapeType?>(null)
    val highlightShapeType: Flow<ShapeType?>
        get() = _highlightShapeType

    private val _selectedTool = MutableStateFlow<ToolType>(Square)
    val selectedTool: Flow<ToolType>
        get() = _selectedTool

    private val _shapes = MutableStateFlow<List<Shape>>(emptyList())
    val shapes: Flow<List<Shape>>
        get() = _shapes

    private val _lines = MutableStateFlow<List<Line>>(emptyList())
    val lines: Flow<List<Line>>
        get() = _lines

    private var dragShape: Shape? = null
    private var dragShapeOffset: Offset = Offset.Zero
    private var initialPressOffset: Offset = Offset.Zero

    fun select(tool: ToolType) {
        _selectedTool.value = tool
    }
    fun add(shape: Shape) {
        _shapes.value = _shapes.value + shape
    }

    suspend fun highlightShape(finger: Offset, shapeBoxSizePx: Float) {
        // find a shape (if any) at the finger location
        _shapes.value.findAt(finger, shapeBoxSizePx)?.let { shape ->
            repeat(3) { n ->
                _highlightShapeType.value = shape.shapeType
                delay(200)
                _highlightShapeType.value = null
                if (n != 2) {
                    delay(200)
                }
            }
        }
        // find all matching shapes and make them blink
    }

    private fun List<Shape>.findAt(offset: Offset, shapeBoxSizePx: Float): Shape? =
        reversed().find { shape ->
            val normalized = offset - shape.offset
            normalized.x >= 0 &&
                    normalized.y >= 0 &&
                    normalized.x <= shapeBoxSizePx &&
                    normalized.y <= shapeBoxSizePx
        }

    fun onPress(finger: Offset) {
        initialPressOffset = finger
    }

    fun dragStart(finger: Offset, size: Float) {
        dragShape = _shapes.value.findAt(finger, size)
        dragShape?.let { shape ->
            dragShapeOffset = initialPressOffset - shape.offset
            drag(finger)
        }
    }

    fun drag(finger: Offset) {
        dragShape?.let { shape ->
            val newShape = shape.copy(offset = finger - dragShapeOffset)
            _shapes.value = _shapes.value - shape + newShape
            dragShape = newShape
        }
    }

    fun dragEnd() {
        dragShape = null
    }

    private var lineInProgress: Line? = null

    fun startLine(finger: Offset, size: Float) {
        Log.d("!!!", "startLine called")
        _shapes.value.findAt(finger, size)?.let { shape ->
            Log.d("!!!", "start shape found")
            lineInProgress = Line(shape.id).apply {
                _lines.value = _lines.value + this
            }
        }
    }

    fun endLine(finger: Offset, size: Float) {
        lineInProgress?.let { line ->
            _shapes.value.findAt(finger, size)?.let { endShape ->
                _lines.value = _lines.value - line + line.copy(shape2Id = endShape.id)
            } ?: run {
                _lines.value = _lines.value - line
            }
            lineInProgress = null
        }
    }
}