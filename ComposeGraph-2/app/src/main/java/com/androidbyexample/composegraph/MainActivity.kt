package com.androidbyexample.composegraph

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.androidbyexample.composegraph.ui.theme.ComposeGraphTheme
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<GraphViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeGraphTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Ui(viewModel)
                }
            }
        }
    }
}

@Composable
fun Ui(
    viewModel: GraphViewModel
) {
    val scope = rememberCoroutineScope()
    val selectedTool by viewModel.selectedTool.collectAsState(initial = Square)
    val highlightShapeType by viewModel.highlightShapeType.collectAsState(initial = null)
    val shapes by viewModel.shapes.collectAsState(initial = emptyList())
    val lines by viewModel.lines.collectAsState(initial = emptyList())
    val handlers = remember(viewModel) {
        Handlers(
            onAddShape = { viewModel.add(it) },
            onToolChange = { viewModel.select(it) },
            onHighlightShape = { finger, size ->
                scope.launch {
                    viewModel.highlightShape(finger, size)
                }
            },
            onDragStart = { finger, size ->
                viewModel.dragStart(finger, size)
            },
            onDrag = { finger ->
                viewModel.drag(finger)
            },
            onDragEnd = {
                viewModel.dragEnd()
            },
            onLineStart = { finger, size ->
                viewModel.startLine(finger, size)
            },
            onLineEnd = { finger, size ->
                viewModel.endLine(finger, size)
            },
            onPress = { finger ->
                viewModel.onPress(finger)
            },
        )
    }

    Graph(
        shapeSizeDp = 36.dp,
        shapeOutlineWidthDp = 3.dp,
        shapeBoxSizeDp = 48.dp,
        shapes = shapes,
        lines = lines,
        selectedTool = selectedTool,
        highlightShapeType = highlightShapeType,
        handlers = handlers,
        modifier = Modifier.fillMaxSize()
    )
}

data class Handlers(
    val onAddShape: (Shape) -> Unit,
    val onToolChange: (ToolType) -> Unit,
    val onHighlightShape: (finger: Offset, size: Float) -> Unit,
    val onDragStart: (finger: Offset, size: Float) -> Unit,
    val onDrag: (finger: Offset) -> Unit,
    val onDragEnd: () -> Unit,
    val onLineStart: (finger: Offset, shapeBoxSize: Float) -> Unit,
    val onLineEnd: (finger: Offset, shapeBoxSize: Float) -> Unit,
    val onPress: (finger: Offset) -> Unit,
)

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Graph(
    shapeSizeDp: Dp,
    shapeOutlineWidthDp: Dp,
    shapeBoxSizeDp: Dp,
    shapes: List<Shape>,
    lines: List<Line>,
    handlers: Handlers,
    selectedTool: ToolType,
    highlightShapeType: ShapeType?,
    modifier: Modifier
) {
    with(LocalDensity.current) {
        val dashLengthPx = 6.dp.toPx()
        val dashGapPx = 3.dp.toPx()
        val shapeSizePx = shapeSizeDp.toPx()
        val shapeOutlineWidthPx = shapeOutlineWidthDp.toPx()
        val shapeBoxSizePx = shapeBoxSizeDp.toPx()
        val halfShapeBoxSizePx = shapeBoxSizePx / 2
        val shapeOffsetPx = (shapeBoxSizePx - shapeSizePx)/2
        val radius = shapeSizePx/2
        val highlightedBorderColor = MaterialTheme.colorScheme.primary
        val normalBorderColor = Color.Black

        val shapeSize = remember(shapeSizePx) {
            Size(shapeSizePx, shapeSizePx)
        }
        val shapeBoxSize = remember(shapeBoxSizePx) {
            Size(shapeBoxSizePx, shapeBoxSizePx)
        }
        val halfShapeBoxOffset = remember(shapeBoxSizePx) {
            Offset(shapeBoxSizePx/2, shapeBoxSizePx/2)
        }
        val shapeCenter = remember(shapeSizePx) {
            Offset(shapeSizePx/2, shapeSizePx/2)
        }
        val outline = remember(shapeOutlineWidthDp) {
            Stroke(shapeOutlineWidthPx)
        }
        val dashedOutline = remember(shapeOutlineWidthDp) {
            Stroke(
                shapeOutlineWidthPx,
                pathEffect = PathEffect.dashPathEffect(floatArrayOf(dashLengthPx, dashGapPx))
            )
        }
        var finger by remember { mutableStateOf(Offset.Zero) }

        val trianglePath = remember(shapeSizePx) {
            Path().apply {
                moveTo(shapeSizePx/2, 0f)
                lineTo(shapeSizePx, shapeSizePx)
                lineTo(0f, shapeSizePx)
                close()
            }
        }
        fun DrawScope.drawTriangle(x: Float, y: Float, borderColor: Color) {
            translate(x + shapeOffsetPx, y + shapeOffsetPx) {
                drawPath(path = trianglePath, color = Color.Red)
                drawPath(path = trianglePath, color = borderColor, style = outline)
            }
        }
        fun DrawScope.drawSquare(x: Float, y: Float, borderColor: Color, fill: Boolean = true, stroke: Stroke= outline) {
            translate(x + shapeOffsetPx, y + shapeOffsetPx) {
                if (fill) {
                    drawRect(
                        color = Color.Blue,
                        topLeft = Offset.Zero,
                        size = shapeSize,
                        style = Fill
                    )
                }
                drawRect(color = borderColor, topLeft = Offset.Zero, size = shapeSize, style = stroke)
            }
        }
        fun DrawScope.drawLine(x: Float, y: Float, borderColor: Color) {
            translate(x, y) {
                val lineY = halfShapeBoxOffset.y
                drawLine(
                    color = borderColor,
                    strokeWidth = shapeOutlineWidthPx,
                    start = Offset(shapeOffsetPx, lineY),
                    end = Offset(shapeBoxSizePx - shapeOffsetPx, lineY)
                )
            }
        }
        fun DrawScope.drawCircle(x: Float, y: Float, borderColor: Color) {
            translate(x + shapeOffsetPx, y + shapeOffsetPx) {
                drawCircle(color = Color.Green, center = shapeCenter, radius = radius, style = Fill)
                drawCircle(color = borderColor, center = shapeCenter, radius = radius, style = outline)
            }
        }

        fun Shape.getCenter(): Offset = offset + halfShapeBoxOffset

        Scaffold(
            topBar = {
                TopAppBar(
                    colors = TopAppBarDefaults.mediumTopAppBarColors(
                        containerColor = MaterialTheme.colorScheme.primary,
                    ),
                    title = { Text(stringResource(R.string.graph)) },
                    actions = {
                        ToolbarButton(
                            toolType = Square,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y, borderColor -> drawSquare(x, y, borderColor) }
                        )
                        ToolbarButton(
                            toolType = Circle,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y, borderColor -> drawCircle(x, y, borderColor) }
                        )
                        ToolbarButton(
                            toolType = Triangle,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y, borderColor -> drawTriangle(x, y, borderColor) }
                        )
                        ToolbarButton(
                            toolType = DrawLine,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y, borderColor -> drawLine(x, y, borderColor) }
                        )
                        ToolbarButton(
                            toolType = Select,
                            selectedTool = selectedTool,
                            onToolChange = handlers.onToolChange,
                            draw = { x, y, borderColor ->
                                drawSquare(x, y, borderColor, fill = false, stroke = dashedOutline)
                            }
                        )
                    }
                )
            },
            content = { paddingValues ->
                val canvasModifier = remember(selectedTool) {
                    modifier
                        .padding(paddingValues)
                        // register tap handler
                        .pointerInput(selectedTool, halfShapeBoxOffset) {
                            detectTapGestures(
                                onTap = {
                                    when (selectedTool) {
                                        is ShapeType -> {
                                            handlers.onAddShape(
                                                Shape(
                                                    shapeType = selectedTool,
                                                    offset = it - halfShapeBoxOffset
                                                )
                                            )
                                        }

                                        Select -> {
                                            handlers.onHighlightShape(it, shapeBoxSizePx)
                                        }

                                        DrawLine -> {} // do nothing on tap
                                    }
                                },
                                onPress = { finger ->
                                    if (selectedTool == Select) {
                                        handlers.onPress(finger)
                                    }
                                },
                            )
                        }
                        // register drag handler
                        .pointerInput(selectedTool, halfShapeBoxOffset) {
                            detectDragGestures(
                                onDragStart = { finger ->
                                    if (selectedTool == DrawLine) {
                                        handlers.onLineStart(finger, shapeBoxSizePx)
                                    } else if (selectedTool == Select) {
                                        handlers.onDragStart(finger, shapeBoxSizePx)
                                    }
                                },
                                onDrag = { change, _ ->
                                    if (selectedTool == DrawLine) {
                                        finger = change.position
                                    } else if (selectedTool == Select) {
                                        handlers.onDrag(change.position)
                                    }
                                },
                                onDragEnd = {
                                    if (selectedTool == DrawLine) {
                                        handlers.onLineEnd(finger, shapeBoxSizePx)
                                    } else if (selectedTool == Select) {
                                        handlers.onDragEnd()
                                    }
                                },
                                onDragCancel = {
                                    if (selectedTool == DrawLine) {
                                        handlers.onLineEnd(finger, shapeBoxSizePx)
                                    } else if (selectedTool == Select) {
                                        handlers.onDragEnd()
                                    }
                                },
                            )
                        }
                    }

                Canvas(
                    modifier = canvasModifier
                ) {
                    lines.forEach { line ->
                        drawLine(
                            color = Color.DarkGray,
                            strokeWidth = shapeOutlineWidthPx,
                            start = shapes.findWithId(line.shape1Id).getCenter(),
                            end = line.shape2Id?.let { shapes.findWithId(line.shape2Id).getCenter() } ?: finger,
                        )
                    }
                    shapes.forEach {
                        val borderColor = if (highlightShapeType == it.shapeType)
                            highlightedBorderColor
                        else
                            normalBorderColor

                        when(it.shapeType) {
                            Circle -> drawCircle(it.offset.x, it.offset.y, borderColor)
                            Square -> drawSquare(it.offset.x, it.offset.y, borderColor)
                            Triangle -> drawTriangle(it.offset.x, it.offset.y, borderColor)
                        }
                    }
//                    val offset = center - Offset(shapeSizePx/2, shapeSizePx/2)
//                    drawTriangle(offset.x, offset.y)
//                    drawTriangle(0f, 0f)
//                    drawTriangle(1000f, 1000f)
//                    drawSquare(0f, 1000f)
//                    drawCircle(1000f, 0f)
                }
            }
        )
    }
}

fun List<Shape>.findWithId(id: String) =
    firstOrNull { it.id == id } ?: throw IllegalStateException("Internal Error: Id $id not found for any shapes")

@Composable
fun ToolbarButton(
    toolType: ToolType,
    selectedTool: ToolType,
    onToolChange: (ToolType) -> Unit,
    draw: DrawScope.(Float, Float, Color) -> Unit
) {
    val borderColor =
        if (selectedTool == toolType) {
            MaterialTheme.colorScheme.onPrimary
        } else {
            MaterialTheme.colorScheme.onPrimaryContainer
        }

    IconButton(onClick = { onToolChange(toolType) }) {
        Canvas(modifier = Modifier.size(48.dp)) {
            draw(0f, 0f, borderColor)
        }
    }
}
