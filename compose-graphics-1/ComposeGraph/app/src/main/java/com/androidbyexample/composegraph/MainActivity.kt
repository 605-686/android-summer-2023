package com.androidbyexample.composegraph

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Fill
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.androidbyexample.composegraph.ui.theme.ComposeGraphTheme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<GraphViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeGraphTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colorScheme.background) {
                    Ui(viewModel)
                }
            }
        }
    }
}

@Composable
fun Ui(
    viewModel: GraphViewModel
) {
    val selectedTool by viewModel.selectedTool.collectAsState(initial = Square)
    val shapes by viewModel.shapes.collectAsState(initial = emptyList())

    Graph(
        shapeSizeDp = 36.dp,
        shapeOutlineWidthDp = 3.dp,
        shapeBoxSizeDp = 48.dp,
        shapes = shapes,
        selectedTool = selectedTool,
        onAddShape = { viewModel.add(it) },
        onToolChange = { viewModel.select(it) },
        modifier = Modifier.fillMaxSize()
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun Graph(
    shapeSizeDp: Dp,
    shapeOutlineWidthDp: Dp,
    shapeBoxSizeDp: Dp,
    shapes: List<Shape>,
    onAddShape: (Shape) -> Unit,
    selectedTool: ToolType,
    onToolChange: (ToolType) -> Unit,
    modifier: Modifier
) {
    with(LocalDensity.current) {
        val shapeSizePx = shapeSizeDp.toPx()
        val shapeOutlineWidthPx = shapeOutlineWidthDp.toPx()
        val shapeBoxSizePx = shapeBoxSizeDp.toPx()
        val shapeOffsetPx = (shapeBoxSizePx - shapeSizePx)/2
        val radius = shapeSizePx/2

        val shapeSize = remember(shapeSizePx) {
            Size(shapeSizePx, shapeSizePx)
        }
        val shapeBoxSize = remember(shapeBoxSizePx) {
            Size(shapeBoxSizePx, shapeBoxSizePx)
        }
        val halfShapeBoxOffset = remember(shapeBoxSizePx) {
            Offset(shapeBoxSizePx/2, shapeBoxSizePx/2)
        }
        val shapeCenter = remember(shapeSizePx) {
            Offset(shapeSizePx/2, shapeSizePx/2)
        }
        val outline = remember(shapeOutlineWidthDp) {
            Stroke(shapeOutlineWidthPx)
        }
        val trianglePath = remember(shapeSizePx) {
            Path().apply {
                moveTo(shapeSizePx/2, 0f)
                lineTo(shapeSizePx, shapeSizePx)
                lineTo(0f, shapeSizePx)
                close()
            }
        }
        fun DrawScope.drawTriangle(x: Float, y: Float) {
            translate(x + shapeOffsetPx, y + shapeOffsetPx) {
                drawPath(path = trianglePath, color = Color.Red)
                drawPath(path = trianglePath, color = Color.Black, style = outline)
            }
        }
        fun DrawScope.drawSquare(x: Float, y: Float) {
            translate(x + shapeOffsetPx, y + shapeOffsetPx) {
                drawRect(color = Color.Blue, topLeft = Offset.Zero, size = shapeSize, style = Fill)
                drawRect(color = Color.Black, topLeft = Offset.Zero, size = shapeSize, style = outline)
            }
        }
        fun DrawScope.drawCircle(x: Float, y: Float) {
            translate(x + shapeOffsetPx, y + shapeOffsetPx) {
                drawCircle(color = Color.Green, center = shapeCenter, radius = radius, style = Fill)
                drawCircle(color = Color.Black, center = shapeCenter, radius = radius, style = outline)
            }
        }

        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text(stringResource(R.string.graph)) },
                    actions = {
                        ToolbarButton(
                            toolType = Square,
                            shapeBoxSize = shapeBoxSize,
                            selectedTool = selectedTool,
                            onToolChange = onToolChange,
                            draw = { x, y -> drawSquare(x, y) }
                        )
                        ToolbarButton(
                            toolType = Circle,
                            shapeBoxSize = shapeBoxSize,
                            selectedTool = selectedTool,
                            onToolChange = onToolChange,
                            draw = { x, y -> drawCircle(x, y) }
                        )
                        ToolbarButton(
                            toolType = Triangle,
                            shapeBoxSize = shapeBoxSize,
                            selectedTool = selectedTool,
                            onToolChange = onToolChange,
                            draw = { x, y -> drawTriangle(x, y) }
                        )
                    }
                )
            },
            content = { paddingValues ->
                Canvas(
                    modifier = modifier
                        .padding(paddingValues)
                        .pointerInput(onAddShape) {
                            detectTapGestures(
                                onTap = {
                                    when(selectedTool) {
                                        is ShapeType -> onAddShape(Shape(shapeType = selectedTool, offset = it - halfShapeBoxOffset))
                                        DrawLine -> TODO()
                                        Select -> TODO()
                                    }
                                }
                            )
                        }
                ) {
                    shapes.forEach {
                        when(it.shapeType) {
                            Circle -> drawCircle(it.offset.x, it.offset.y)
                            Square -> drawSquare(it.offset.x, it.offset.y)
                            Triangle -> drawTriangle(it.offset.x, it.offset.y)
                        }
                    }
//                    val offset = center - Offset(shapeSizePx/2, shapeSizePx/2)
//                    drawTriangle(offset.x, offset.y)
//                    drawTriangle(0f, 0f)
//                    drawTriangle(1000f, 1000f)
//                    drawSquare(0f, 1000f)
//                    drawCircle(1000f, 0f)
                }
            }
        )
    }
}

@Composable
fun ToolbarButton(
    toolType: ToolType,
    shapeBoxSize: Size,
    selectedTool: ToolType,
    onToolChange: (ToolType) -> Unit,
    draw: DrawScope.(Float, Float) -> Unit
) {
    IconButton(onClick = { onToolChange(toolType) }) {
        Canvas(modifier = Modifier.size(48.dp)) {
            if (selectedTool == toolType) {
                drawRect(color = Color.White, topLeft = Offset.Zero, size = shapeBoxSize)
            }
            draw(0f,0f)
        }
    }
}
