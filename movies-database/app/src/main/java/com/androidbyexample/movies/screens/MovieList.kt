package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import com.androidbyexample.movies.components.SimpleText
import com.androidbyexample.movies.repository.MovieDto

@Composable
fun MovieList(
    movies: List<MovieDto>
) {
    Column {
        SimpleText(text = "Movies")
        movies.forEach {
            SimpleText(text = it.title)
        }
    }
}
