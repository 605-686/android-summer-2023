package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import com.androidbyexample.movies.components.SimpleText
import com.androidbyexample.movies.repository.ActorDto

@Composable
fun ActorList(
    actors: List<ActorDto>
) {
    Column {
        SimpleText(text = "Actors")
        actors.forEach {
            SimpleText(text = it.name)
        }
    }
}
