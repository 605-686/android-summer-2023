package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.androidbyexample.movies.ActorListScreen
import com.androidbyexample.movies.MovieListScreen
import com.androidbyexample.movies.MovieViewModel
import com.androidbyexample.movies.RatingListScreen
import com.androidbyexample.movies.RatingScreen
import com.androidbyexample.movies.components.SimpleButton

@Composable
fun TestUI(
    viewModel: MovieViewModel
) {
    // This is a hideous, but simple simple test screen
    //   (There are some bad things happening here... We'll fix it!)
    // Don't worry about how it does its job; we'll cover that soon!
    Column {
        Row {
            SimpleButton("Reset") {
                viewModel.resetDatabase()
            }
            SimpleButton("Ratings") {
                viewModel.switchTo(RatingListScreen)
            }
        }
        Row {
            SimpleButton("Movies") {
                viewModel.switchTo(MovieListScreen)
            }
            SimpleButton("Actors") {
                viewModel.switchTo(ActorListScreen)
            }
        }

        val ratings by viewModel.ratingsFlow.collectAsState(initial = emptyList())
        val movies by viewModel.moviesFlow.collectAsState(initial = emptyList())
        val actors by viewModel.actorsFlow.collectAsState(initial = emptyList())

        when(val screen = viewModel.screen) {
            RatingListScreen -> RatingList(ratings = ratings) { id ->
                viewModel.switchTo(RatingScreen(id))
            }
            MovieListScreen -> MovieList(movies = movies)
            ActorListScreen -> ActorList(actors = actors)
            is RatingScreen -> RatingDisplay(
                ratingId = screen.id,
                fetchRatingWithMovies = { id ->
                    viewModel.getRatingWithMovies(id)
                }
            )
        }
    }
}
