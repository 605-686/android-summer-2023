package com.androidbyexample.movies

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.androidbyexample.movies.repository.MovieDatabaseRepository
import com.androidbyexample.movies.repository.MovieRepository
import kotlinx.coroutines.launch

sealed interface Screen
object RatingListScreen: Screen
object MovieListScreen: Screen
object ActorListScreen: Screen
data class RatingScreen(
    val id: String
): Screen

class MovieViewModel(
    private val repository: MovieRepository
) : ViewModel() {
    var screen by mutableStateOf<Screen>(RatingListScreen)
        private set

    val ratingsFlow = repository.ratingsFlow
    val moviesFlow = repository.moviesFlow
    val actorsFlow = repository.actorsFlow

    suspend fun getRatingWithMovies(id: String) =
        repository.getRatingWithMovies(id)

    fun switchTo(screen: Screen) {
        this.screen = screen
    }

    fun resetDatabase() {
        viewModelScope.launch {
            repository.resetDatabase()
        }
    }

    companion object {
        val Factory: ViewModelProvider.Factory = object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(
                modelClass: Class<T>,
                extras: CreationExtras
            ): T {
                // Get the Application object from extras
                val application = checkNotNull(extras[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY])
                return MovieViewModel(
                    MovieDatabaseRepository.create(application)
                ) as T
            }
        }
    }
}