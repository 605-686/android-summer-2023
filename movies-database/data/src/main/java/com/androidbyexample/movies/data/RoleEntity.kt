package com.androidbyexample.movies.data

import androidx.room.Entity
import androidx.room.ForeignKey

@Entity(
    primaryKeys = ["actorId", "movieId"],
    foreignKeys = [
        ForeignKey(
            entity = MovieEntity::class,
            parentColumns = ["id"],
            childColumns = ["movieId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        ),
        ForeignKey(
            entity = ActorEntity::class,
            parentColumns = ["id"],
            childColumns = ["actorId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        )
    ]
)
data class RoleEntity(
    var movieId: String,
    var actorId: String,
    var character: String,
    var orderInCredits: Int,
)