package com.androidbyexample.movies.screens

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.androidbyexample.movies.components.SimpleText
import com.androidbyexample.movies.repository.MovieWithCastDto
import com.androidbyexample.movies.repository.RatingWithMoviesDto

@Composable
fun MovieDisplay(
    movieWithCastDto: MovieWithCastDto?,
) {
    if (movieWithCastDto == null) {
        SimpleText(text = "loading")
    } else {
        SimpleText(text = movieWithCastDto.movie.title)
        movieWithCastDto.cast.forEach { roleWithActorDto ->
            SimpleText(text = "${roleWithActorDto.character} - ${roleWithActorDto.actor.name}")
        }
    }
}