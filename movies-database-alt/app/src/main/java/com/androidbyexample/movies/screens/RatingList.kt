package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import com.androidbyexample.movies.components.SimpleText
import com.androidbyexample.movies.repository.RatingDto

@Composable
fun RatingList(
    ratings: List<RatingDto>,
    onRatingClick: (String) -> Unit,
) {
    Column {
        SimpleText(text = "Ratings")
        ratings.forEach {
            SimpleText(text = it.name) {
                onRatingClick(it.id)
            }
        }
    }
}
