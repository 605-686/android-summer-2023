package com.example.legacy.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {
    private val movies1 = listOf(
        "Hobbs and Shaw",
        "Jumanji: Welcome to the Jungle",
        "The Transporter",
        "Transporter 2",
    )
    // simulate deletion
    private val movies2 = listOf(
        "Hobbs and Shaw",
        "Transporter 2",
    )

    fun deleteSelectedMovies() {
        _movies.value = movies2
    }
    private val _movies = MutableLiveData<List<String>>().apply {
        value = movies1
    }
    val movies: LiveData<List<String>> = _movies

//
//
//
//    fun changeText() {
//        _text.value = "Foo Foo Foo"
//    }
//    private val _text = MutableLiveData<String>().apply {
//        value = "This is home Fragment"
//    }
//    val text: LiveData<String> = _text
}