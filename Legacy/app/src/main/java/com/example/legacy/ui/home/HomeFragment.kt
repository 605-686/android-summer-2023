package com.example.legacy.ui.home

import android.os.Bundle
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.legacy.R
import com.example.legacy.databinding.FragmentHomeBinding
import com.example.legacy.databinding.MovieRowBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val adapter = MovieAdapter()
    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        viewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val recyclerView: RecyclerView = binding.root
        recyclerView.adapter = adapter

        viewModel.movies.observe(viewLifecycleOwner) {
            adapter.movies = it
        }
        return recyclerView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.delete_action -> {
                viewModel.deleteSelectedMovies()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

class MovieViewHolder(private val binding: MovieRowBinding): RecyclerView.ViewHolder(binding.root) {
    fun bind(movie: String) {
        binding.title.text = movie
    }
}

class MovieAdapter: RecyclerView.Adapter<MovieViewHolder>() {
    var movies: List<String> = emptyList()
        set(value) {
            val callback = MovieCallback(field, value)
            val result = DiffUtil.calculateDiff(callback)
            field = value
            result.dispatchUpdatesTo(this)
//            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = MovieRowBinding.inflate(inflater)
        return MovieViewHolder(binding)
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]
        holder.bind(movie)
    }
}

class MovieCallback(
    private val oldMovies: List<String>,
    private val newMovies: List<String>,
): DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldMovies.size
    override fun getNewListSize(): Int = newMovies.size
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldMovies[oldItemPosition] === newMovies[newItemPosition]
    }
    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldMovies[oldItemPosition] == newMovies[newItemPosition]
    }
}