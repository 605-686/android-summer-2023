package com.example.legacy.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DashboardViewModel : ViewModel() {
    private val movie1 = "Hobbs and Shaw"
    private val movie2 = "Transporter 2"

    private val _movie = MutableLiveData<String>().apply {
        value = movie1
    }
    val movie: LiveData<String> = _movie

    fun changeMovie() {
        _movie.value = movie2
    }
}