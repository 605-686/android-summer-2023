package com.javadude.testing

import app.cash.turbine.test
import io.mockk.every
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.BeforeClass

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    companion object {
        @JvmStatic
        @BeforeClass
        fun overallSetup() {
            //  run ONCE before all tests
        }
    }

    @Before
    fun setup() {
        // run before EVERY test
        // test order is NOT specified
    }
    @After
    fun cleanup() {
        // run before EVERY test
        // test order is NOT specified
    }

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
    @Test
    fun subtraction_isCorrect() {
        assertEquals(5, 7 - 2)
    }

    @Test
    fun test_view_model() {
        val repository = mockk<MovieRepository>()

        every { repository.getMovies() } returns flow {
            emit(listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart")))
        }

        justRun { repository.saveMovie(any()) }

        val viewModel = MovieViewModel(repository)

        runBlocking {
            viewModel.movies.test {
                verify { repository.getMovies() }
                assertEquals(listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart")), awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }

        val toSave = Movie("m3", "Romancing the Stone")
        viewModel.saveMovie(toSave)
        verify { repository.saveMovie(toSave) }
    }
}