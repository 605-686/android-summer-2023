package com.javadude.testing

import android.content.Context
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.semantics.SemanticsProperties
import androidx.compose.ui.semantics.heading
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.test.assertAny
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.filter
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.isHeading
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onChildAt
import androidx.compose.ui.test.onChildren
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.text.style.TextAlign
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import app.cash.turbine.test
import com.javadude.testing.ui.theme.TestingTheme
import kotlinx.coroutines.runBlocking

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule
import kotlin.time.ExperimentalTime

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun use_app_context() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.testing", appContext.packageName)
    }

    @get:Rule
    val composeRule = createComposeRule()

    @Test
    fun test_simple_compose_ui1() {
        composeRule.setContent {
            var text by remember { mutableStateOf("Hello") }
            TestingTheme {
                Text(
                    text = text,
                    textAlign = TextAlign.Center,
                    modifier =
                        Modifier
                            .fillMaxSize()
                            .testTag("label")
// testTag is shorthand for
//                            .semantics {
//                                set(SemanticsProperties.TestTag, "label")
//                            }
                            .clickable {
                            text = "Goodbye"
                        }
                )
            }
        }

//        composeRule.onNodeWithTag("label").assertTextEquals("Hello")
//        composeRule.onNodeWithTag("label").performClick()
//        composeRule.onNodeWithTag("label").assertTextEquals("Goodbye")
        composeRule
            .onNodeWithTag("label")
            .run {
                assertTextEquals("Hello")
                performClick()
                assertTextEquals("Goodbye")
            }
//        with(composeRule.onNodeWithTag("label")) {
//            assertTextEquals("Hello")
//            performClick()
//            assertTextEquals("Goodbye")
//        }
    }


    @Test
    fun test_simple_compose_ui2() {
        composeRule.setContent {
            TestingTheme {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .testTag("stuff")
                ) {
                    Text(
                        text = "Name",
                        style = MaterialTheme.typography.titleSmall,
                        modifier = Modifier
                            .fillMaxWidth()
                            .semantics {
                                heading()
                            }
                            .testTag("label")
                    )
                    Text(
                        text = "Scott",
                        style = MaterialTheme.typography.headlineMedium,
                        modifier = Modifier
                            .fillMaxWidth()
                            .testTag("user name")
                    )
                }
            }
        }

        composeRule.onNodeWithTag("label").assertTextEquals("Name")
        composeRule.onNodeWithTag("user name").assertTextEquals("Scott")
        composeRule.onNodeWithTag("stuff").onChildAt(0).assertTextEquals("Name")
        composeRule.onNodeWithTag("stuff").onChildAt(1).assertTextEquals("Scott")

        composeRule
            .onNodeWithTag("stuff")
            .onChildren()
            .filter(isHeading())
            .assertAny(hasText("Name"))

    }

    @Test
    fun checkBaseMovies() {
        val movies = listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart"))
        composeRule.setContent {
            TestingTheme {
                Movies(movies, onReset = {})
            }
        }

        composeRule
            .onNodeWithTag("moviesColumn")
            .onChildAt(1)
            .assertTextEquals("Movies")

        movies.forEachIndexed { n, movie ->
            composeRule
                .onNodeWithTag("moviesColumn")
                .onChildAt(n+2)
                .assertTextEquals(movie.title)
        }

//        with(composeRule.onNodeWithTag("moviesColumn")) {
//            onChildAt(1).assertTextEquals("Movies")
//            movies.forEachIndexed { n, movie ->
//                onChildAt(n+2).assertTextEquals(movie.title)
//            }
//        }
//        composeRule.onNodeWithTag("moviesColumn").run() {
//            onChildAt(1).assertTextEquals("Movies")
//            movies.forEachIndexed { n, movie ->
//                onChildAt(n+2).assertTextEquals(movie.title)
//            }
//        }
    }

    @Test
    fun checkBaseMoviesUsingViewModel() {
        val movies = listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart"))
        val context = ApplicationProvider.getApplicationContext<Context>()
        val dao = Room
            .inMemoryDatabaseBuilder(context, MovieDatabase::class.java)
            .addCallback(object: RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m1', 'Die Hard')")
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m2', 'Get Smart')")
                }
            })
            .build()
            .dao
        val repository = MovieRepository(dao)
        val viewModel = MovieViewModel(repository)

        composeRule.setContent {
            TestingTheme {
                Ui(viewModel = viewModel)
            }
        }

//        composeRule
//            .onNodeWithTag("moviesColumn")
//            .onChildAt(1)
//            .assertTextEquals("Movies")
//
//        movies.forEachIndexed { n, movie ->
//            composeRule
//                .onNodeWithTag("moviesColumn")
//                .onChildAt(n+2)
//                .assertTextEquals(movie.title)
//        }
//
//        with(composeRule.onNodeWithTag("moviesColumn")) {
//            onChildAt(1).assertTextEquals("Movies")
//            movies.forEachIndexed { n, movie ->
//                onChildAt(n+2).assertTextEquals(movie.title)
//            }
//        }
        composeRule.onNodeWithTag("moviesColumn").run {
            onChildAt(1).assertTextEquals("Movies")
            movies.forEachIndexed { n, movie ->
                onChildAt(n+2).assertTextEquals(movie.title)
            }
        }
    }

    @ExperimentalTime
    @Test
    fun testDatabase() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val database = Room
            .inMemoryDatabaseBuilder(context, MovieDatabase::class.java)
            .addCallback(object: RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m1', 'Die Hard')")
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m2', 'Get Smart')")
                }
            })
            .build()

        runBlocking {
            database.dao.getMovies().test {
                assertEquals(listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart")), awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }

    @ExperimentalTime
    @Test
    fun testRepository() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        val dao = Room
            .inMemoryDatabaseBuilder(context, MovieDatabase::class.java)
            .addCallback(object: RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m1', 'Die Hard')")
                    db.execSQL("INSERT INTO Movie(id, title) VALUES('m2', 'Get Smart')")
                }
            })
            .build()
            .dao

        val repository = MovieRepository(dao)

        runBlocking {
            repository.getMovies().test {
                assertEquals(listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart")), awaitItem())
                cancelAndIgnoreRemainingEvents()
            }
        }
    }
}