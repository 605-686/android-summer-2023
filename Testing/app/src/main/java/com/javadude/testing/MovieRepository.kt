package com.javadude.testing

import android.content.Context
import androidx.room.Room
import kotlinx.coroutines.flow.emptyFlow

open class MovieRepository(private val dao: MovieDAO? = null) {
    fun getMovies() = dao?.getMovies() ?: emptyFlow()
    fun saveMovie(movie: Movie) { /* update db */ }
    suspend fun resetDatabase() { dao?.resetDatabase() }
    companion object {
        fun create(context: Context) =
            MovieRepository(createDao(context))
    }
}

fun createDao(context: Context) =
    Room.databaseBuilder(context, MovieDatabase::class.java, "MOVIES")
        .build() // create database instance
        .dao

fun createInMemoryDB(context: Context) =
    Room.inMemoryDatabaseBuilder(
        context,
        MovieDatabase::class.java
    ).build()
