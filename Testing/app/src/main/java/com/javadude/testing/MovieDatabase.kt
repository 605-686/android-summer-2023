package com.javadude.testing

import androidx.room.Database
import androidx.room.RoomDatabase
import com.javadude.testing.MovieDAO

@Database(version = 1, entities = [Movie::class])
abstract class MovieDatabase: RoomDatabase() {
    abstract val dao: MovieDAO
}