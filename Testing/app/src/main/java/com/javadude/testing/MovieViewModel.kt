package com.javadude.testing

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras

class MovieViewModel(private val repository: MovieRepository): ViewModel() {
    val movies = repository.getMovies()
    fun saveMovie(movie: Movie) = repository.saveMovie(movie)
    fun createMovie(title: String) = Movie(title = title)
    suspend fun resetDatabase() {
        repository.resetDatabase()
    }
    companion object {
        val Factory: ViewModelProvider.Factory = object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(
                modelClass: Class<T>,
                extras: CreationExtras
            ): T {
                // Get the Application object from extras
                val application = checkNotNull(extras[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY])
                return MovieViewModel(
                    MovieRepository.create(application)
                ) as T
            }
        }
    }
}