package com.javadude.testing

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDAO {
    @Query("SELECT * FROM Movie ORDER BY TITLE")
    fun getMovies(): Flow<List<Movie>>

    @Update
    fun saveMovie(vararg movie: Movie)

    @Insert
    fun createMovie(vararg movie: Movie)

    @Query("DELETE FROM Movie")
    fun clearMovies()

    @Transaction
    open suspend fun resetDatabase() {
        clearMovies()
        createMovie(
            Movie("m1", "The Transporter"),
            Movie("m2", "Transporter 2"),
            Movie("m3", "Hobbs and Shaw"),
            Movie("m4", "Jumanji - Welcome to the Jungle"),
        )
    }

}