package com.javadude.testing

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import com.javadude.testing.ui.theme.TestingTheme
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    private val viewModel: MovieViewModel by viewModels { MovieViewModel.Factory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TestingTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Ui(viewModel)
                }
            }
        }
    }
}

@Composable
fun Ui(
    viewModel: MovieViewModel,
    modifier: Modifier = Modifier,
) {
    val movies by viewModel.movies.collectAsState(initial = emptyList())
    Movies(
        movies,
        onReset = viewModel::resetDatabase
    )
}

@Composable
fun Movies(
    movies: List<Movie>,
    onReset: suspend () -> Unit,
    modifier: Modifier = Modifier
) {
    val scope = rememberCoroutineScope()
    Column(
        modifier = modifier.fillMaxWidth().testTag("moviesColumn")
    ) {
        Button(
            onClick = {
                scope.launch { onReset() }
            },
            modifier = Modifier.testTag("reset button")
        ) {
            Text(text = "Reset")
        }
        Text(text = "Movies")
        movies.forEach {
            Text(text = it.title)
        }
    }
}


@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Helloxxxx $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview1() {
    TestingTheme {
        Greeting("Androidaaa")
    }
}
@Preview(showBackground = true)
@Composable
fun GreetingPreview2() {
    TestingTheme {
        Greeting("Android")
    }
}
@Preview(showBackground = true)
@Composable
fun GreetingPreview3() {
    TestingTheme {
        Greeting("Android1231")
    }
}