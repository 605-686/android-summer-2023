# Changes made after class session

## Dead spot when clicking on cards     

Removed the onClick from `SimpleText`
  * was swallowing clicks on the text itself, creating a dead-spot
  * Added clickable modifier to list items in display screens

## Problem scenario I mentioned

  1. Delete Jason Statham
  1. Display The Transporter
  1. Press refresh

* Database is updated
* Actors are not updated on the movie display screen
  * (If you exit movie display and re-enter you'll see the change)

## For your assignment

This will be similar to, on the contact edit screen, address list will not refresh when deleting addresses if you use the `LaunchedEffect` approach.

## Solution

Use a `Flow` to fetch the `MovieWithCastDto` (instead of having `MovieDisplay` do the fetch).

When the movie or cast changes in the database it will emit a new entry, which will force the refresh.

## Necessary changes
Note: All code changes are surrounded with 

```kotlin
// BEGIN/END ADD/REMOVE FOR UPDATE"
```

comments

### MovieDisplay

Explicitly pass in `movieWithCastDto` as parameter
  * a caller will fetch it
  * removed `movieWithCastDto` "bucket" and `LaunchedEffect` to fetch the movie
  * removed `movieId` and `fetchMovieWithCast` parameter
  * added `movieWithCastDto` as parameter
  * no longer need the "capture to enable smart cast" - it's a fixed parameter now - clean up

```kotlin
@Composable
fun MovieDisplay(
    movieWithCastDto: MovieWithCastDto?, // ADDED
    //REMOVED movieId
    //REMOVED fetchMovieWithCast
    ...
) {
    //REMOVED var movieWithCastDto
    //REMOVED LaunchedEffect

    MovieScaffold(
        ...
    ) { paddingValues ->
        //REMOVED val movieDto = movieWithCastDto
        //CLEANED UP REFS BELOW

        ...
    }
}

```

### MovieDao

Added
    
```kotlin
@Transaction
@Query("SELECT * FROM MovieEntity WHERE id = :id")
abstract fun getMovieWithCastFlow(id: String): Flow<MovieWithCast>
```

This will re-emit to the `Flow` whenever the movie or its cast changes

### MovieRepository and MovieDatabaseRepository

Added pass-throughs for `getMovieWithCastFlow`

In `MovieRepository`

```kotlin
fun getMovieWithCastFlow(id: String): Flow<MovieWithCastDto>
```

In `MovieDatabaseRepository`

```kotlin
override fun getMovieWithCastFlow(id: String): Flow<MovieWithCastDto> =
    dao.getMovieWithCastFlow(id).map { movieWithCast: MovieWithCast ->
        movieWithCast.toDto()
    }
```

### MovieViewModel

Added

```kotlin
fun getMovieWithCastFlow(id: String) =
    repository.getMovieWithCastFlow(id)
```

### Ui

When creating `MovieDisplay`

  1. Call `getMovieWithCastFlow` from `viewModel`
  1. Collect it
  1. Pass it to the `MovieDisplay`

```kotlin
is MovieDisplayScreen -> {
    val movieWithCastDto by                     // ADDED
        viewModel                               // ADDED
            .getMovieWithCastFlow(screen.id)    // ADDED
            .collectAsState(initial = null)     // ADDED
    MovieDisplay(
        movieWithCastDto = movieWithCastDto,    // ADDED
        //REMOVED movieId
        //REMOVED fetchMovieWithCast
        ...
    )
}

```

After this change, the problem no longer exists (for Movies)

