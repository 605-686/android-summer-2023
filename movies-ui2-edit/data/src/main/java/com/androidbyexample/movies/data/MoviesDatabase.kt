package com.androidbyexample.movies.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    version = 1,
    entities = [
        MovieEntity::class,
        ActorEntity::class,
        RoleEntity::class,
        RatingEntity::class,
    ],
    exportSchema = false
)
abstract class MoviesDatabase: RoomDatabase() {
    abstract val dao: MovieDao
    // or abstract fun getDao(): MovieDao
}