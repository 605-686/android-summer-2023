package com.androidbyexample.movies.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.R
import com.androidbyexample.movies.Screen
import com.androidbyexample.movies.components.MovieScaffold
import com.androidbyexample.movies.components.SimpleText
import com.androidbyexample.movies.repository.RatingWithMoviesDto

@Composable
fun RatingDisplay(
    ratingId: String,
    fetchRatingWithMovies: suspend (String) -> RatingWithMoviesDto,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onMovieClick: (String) -> Unit,
) {
    var ratingWithMoviesDto by remember { mutableStateOf<RatingWithMoviesDto?>(null) }

    LaunchedEffect(key1 = ratingId) {
        // starts a coroutine to fetch the rating
        ratingWithMoviesDto = fetchRatingWithMovies(ratingId)
    }

    MovieScaffold(
        title = ratingWithMoviesDto?.rating?.name ?: stringResource(id = R.string.loading),
        onSelectListScreen = onSelectListScreen,
        onResetDatabase = onResetDatabase,
    ) { paddingValues ->
        ratingWithMoviesDto?.let { ratingWithMoviesDto ->
            ratingWithMoviesDto
                .takeIf { it.movies.isNotEmpty() }
                ?.also { rating ->
                    Column(modifier = Modifier.padding(paddingValues)) {
                        SimpleText(text = stringResource(id = R.string.movies, rating.rating.name))

                        LazyColumn(
                            modifier = Modifier
                                .padding(start = 8.dp)
                                .weight(1f)
                        ) {
                            items(
                                items = ratingWithMoviesDto.movies
                            ) { movie ->
                                Card(
                                    elevation = CardDefaults.cardElevation(),
                                    modifier = Modifier
                                        .padding(8.dp)
                                        .fillMaxWidth()
                                        .clickable {
                                            onMovieClick(movie.id)
                                        }
                                ) {
                                    SimpleText(text = movie.title)
                                }
                            }
                        }
                    }
                }
                ?: run {
                    SimpleText(
                        text = stringResource(id = R.string.no_movies_found_for_this_rating),
                        modifier = Modifier.padding(paddingValues)
                    )
                }
        }
    }
}
