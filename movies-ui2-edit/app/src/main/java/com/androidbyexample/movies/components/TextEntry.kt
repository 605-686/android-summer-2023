package com.androidbyexample.movies.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TextEntry(
    @StringRes labelId: Int,
    @StringRes placeholderId: Int,
    value: String?,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    var theValue by remember(value) {
        mutableStateOf(value)
    }
    OutlinedTextField(
        label = {
            Text(
                text = stringResource(id = labelId),
            )
        },
        placeholder = {
            Text(
                text =
                if (theValue == null) {
                    "loading..."
                } else {
                    stringResource(id = placeholderId)
                }
            )
        },
        value = theValue ?: "",
        onValueChange = {
            theValue = it // update for the displayed field
            if (value != null) {
                onValueChange(it)
            }
        },
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp)
    )
}