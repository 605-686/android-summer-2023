package com.androidbyexample.movies.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.androidbyexample.movies.R
import com.androidbyexample.movies.Screen
import com.androidbyexample.movies.components.MovieScaffold
import com.androidbyexample.movies.components.TextEntry
import com.androidbyexample.movies.repository.MovieDto
import com.androidbyexample.movies.repository.MovieWithCastDto

@Composable
fun MovieEdit(
    movieWithCastDto: MovieWithCastDto?,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onMovieUpdate: (MovieDto) -> Unit,
) {
    val movie = movieWithCastDto?.movie
    MovieScaffold(
        title = movie?.title ?: stringResource(id = R.string.loading),
        onSelectListScreen = onSelectListScreen,
        onResetDatabase = onResetDatabase,
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .padding(paddingValues)
                .fillMaxHeight()
        ) {
            TextEntry(
                labelId = R.string.title,
                placeholderId = R.string.movie_title_placeholder,
                value = movie?.title ?: "",
                onValueChange = {
                    movie?.copy(title = it)?.apply {
                        onMovieUpdate(this)
                    }
                },
            )
            TextEntry(
                labelId = R.string.description,
                placeholderId = R.string.movie_description_placeholder,
                value = movie?.description ?: "",
                onValueChange = {
                    movie?.copy(description = it)?.apply {
                        onMovieUpdate(this)
                    }
                },
            )
        }
    }
}
