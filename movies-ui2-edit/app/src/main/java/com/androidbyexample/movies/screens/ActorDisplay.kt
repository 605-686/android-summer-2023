package com.androidbyexample.movies.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.R
import com.androidbyexample.movies.Screen
import com.androidbyexample.movies.components.MovieScaffold
import com.androidbyexample.movies.components.SimpleText
import com.androidbyexample.movies.repository.ActorWithFilmographyDto

@Composable
fun ActorDisplay(
    actorId: String,
    fetchActorWithFilmography: suspend (String) -> ActorWithFilmographyDto,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    onMovieClick: (String) -> Unit,
) {
    var actorWithFilmographyDto by remember { mutableStateOf<ActorWithFilmographyDto?>(null) }

    LaunchedEffect(key1 = actorId) {
        // starts a coroutine to fetch the rating
        actorWithFilmographyDto = fetchActorWithFilmography(actorId)
    }

    MovieScaffold(
        title = actorWithFilmographyDto?.actor?.name ?: stringResource(id = R.string.loading),
        onSelectListScreen = onSelectListScreen,
        onResetDatabase = onResetDatabase,
    ) { paddingValues ->
        val actor = actorWithFilmographyDto // capture to enable smart cast

        if (actor == null || actor.filmography.isEmpty()) {
            SimpleText(
                text = stringResource(id = R.string.no_movies_found_for_this_actor),
                modifier = Modifier.padding(paddingValues)
            )
        } else {
            Column(modifier = Modifier.padding(paddingValues)) {
                SimpleText(text = stringResource(id = R.string.filmography, actor.actor.name))
                LazyColumn(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 8.dp)
                        .weight(1f)
                ) {
                    items(
                        items = actorWithFilmographyDto?.filmography ?: emptyList()
                    ) { roleWithMovie ->
                        Card(
                            elevation = CardDefaults.cardElevation(),
                            modifier = Modifier
                                .padding(8.dp)
                                .fillMaxWidth()
                                .clickable {
                                    onMovieClick(roleWithMovie.movie.id)
                                }
                        ) {
                            SimpleText(
                                text = "${roleWithMovie.movie.title} (${roleWithMovie.character})",
                            )
                        }
                    }
                }
            }
        }
    }
}
