// determine from whence plugins are loaded
pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    // pull in the androidbyexample version catalog to resolve
    //   plugin versions and version numbers such as minSdk and compose compiler
    versionCatalogs {
        create("libs") {
            from("com.androidbyexample:version-catalog:1.0")
        }
    }

    // don't allow our modules to list repositories; all repositories
    //   are listed here for all modules
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        mavenLocal()
    }
}

rootProject.name = "Sample"
include(":app")