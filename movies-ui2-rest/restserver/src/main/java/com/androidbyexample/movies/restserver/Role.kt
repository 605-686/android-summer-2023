package com.androidbyexample.movies.restserver

import com.fasterxml.jackson.annotation.JsonProperty
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement
class Role(
    @JsonProperty("movieId") var movieId: String,
    @JsonProperty("actorId") var actorId: String,
    @JsonProperty("character") var character: String,
    @JsonProperty("orderInCredits") var orderInCredits: Int
)

@XmlRootElement
class ExpandedRole(
    @JsonProperty("movie") var movie: Movie,
    @JsonProperty("actor") var actor: Actor,
    @JsonProperty("character") var character: String,
    @JsonProperty("orderInCredits") var orderInCredits: Int
)