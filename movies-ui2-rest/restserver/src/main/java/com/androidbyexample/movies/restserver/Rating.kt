package com.androidbyexample.movies.restserver

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement
data class Rating(
    @JsonProperty("id") val id: String = UUID.randomUUID().toString(),
    @JsonProperty("name") val name: String,
    @JsonProperty("description") val description: String
)

@XmlRootElement
data class RatingWithMovies(
    @JsonProperty("rating") var rating: Rating,
    @JsonProperty("movies") var movies: List<Movie>,
)