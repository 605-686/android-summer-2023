package com.androidbyexample.movies.restserver

import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response

private fun <T> response(status: Response.Status, entity: T) =
    Response.status(status).entity(entity).build()

private fun <T> ok(entity: T) =
    response(Response.Status.OK, entity)

private fun <T> notFound(entity: T) =
    response(Response.Status.NOT_FOUND, entity)

private fun <T> created(entity: T) =
    response(Response.Status.CREATED, entity)


private val moviesByIdIndex = mutableMapOf<String, Movie>()
private val actorsByIdIndex = mutableMapOf<String, Actor>()
private val rolesByMovieIdIndex = mutableMapOf<String, MutableList<Role>>()
private val rolesByActorIdIndex = mutableMapOf<String, MutableList<Role>>()
private val moviesByRatingIdIndex = mutableMapOf<String, MutableList<Movie>>()

private val notFoundRating = Rating("-", "NOT FOUND", "NOT FOUND")
private val notFoundMovie = Movie("-", "NOT FOUND", "NOT FOUND", "--")
private val notFoundMovieWithRoles = MovieWithCast(notFoundMovie, emptyList())
private val notFoundActor = Actor("-", "NOT FOUND")
private val notFoundActorWithRoles = ActorWithFilmography(notFoundActor, emptyList())
private val notFoundRatingWithMovies = RatingWithMovies(notFoundRating, emptyList())

@Path("/")
class RestController {
    @GET
    @Path("rating")
    @Produces(MediaType.APPLICATION_JSON)
    fun getRatings(): Response = ok(ratings)

    @GET
    @Path("movie")
    @Produces(MediaType.APPLICATION_JSON)
    fun getMovies(): Response {
        val movies = moviesByIdIndex.values.sortedBy { it.title }
        return ok(movies)
    }

    @GET
    @Path("actor")
    @Produces(MediaType.APPLICATION_JSON)
    fun getActors(): Response = ok(actorsByIdIndex.values.sortedBy { it.name })

    @GET
    @Path("rating/{id}/movies")
    @Produces(MediaType.APPLICATION_JSON)
    suspend fun getRatingWithMovies(@PathParam("id") id: String): RatingWithMovies =
        ratingsIndex[id]?.let { rating ->
            val movies = moviesByRatingIdIndex[id] ?: emptyList()
            RatingWithMovies(rating, movies)
        } ?: notFoundRatingWithMovies

    @GET
    @Path("movie/{id}/roles")
    @Produces(MediaType.APPLICATION_JSON)
    suspend fun getMovieWithRoles(@PathParam("id") id: String): MovieWithCast =
        moviesByIdIndex[id]?.let { movie ->
            val roles =
                rolesByMovieIdIndex[id]
                    ?.map { role ->
                        RoleWithActor(
                            actor = actorsByIdIndex[role.actorId] ?: throw IllegalStateException(),
                            character = role.character,
                            orderInCredits = role.orderInCredits,
                        )
                    }
                    ?: emptyList()

            MovieWithCast(movie, roles)
        } ?: notFoundMovieWithRoles

    @GET
    @Path("foo")
    @Produces(MediaType.APPLICATION_JSON)
    fun foo(): String {
        return "hello"
    }

    @GET
    @Path("actor/{id}/roles")
    @Produces(MediaType.APPLICATION_JSON)
    suspend fun getActorWithRoles(@PathParam("id") id: String): ActorWithFilmography =
        actorsByIdIndex[id]?.let { actor ->
            val roles =
                rolesByActorIdIndex[id]
                    ?.map { role ->
                        RoleWithMovie(
                            movie = moviesByIdIndex[role.movieId] ?: throw IllegalStateException(),
                            character = role.character,
                            orderInCredits = role.orderInCredits,
                        )
                    }
                    ?: emptyList()
            ActorWithFilmography(actor, roles)
        } ?: notFoundActorWithRoles

    private fun insertMovies(vararg newMovies: Movie) {
        newMovies.forEach { movie ->
            moviesByIdIndex[movie.id] = movie
            moviesByRatingIdIndex.getOrCreate(movie.ratingId).add(movie)
        }
    }

    private fun insertActors(vararg newActors: Actor) {
        newActors.forEach { actor ->
            actorsByIdIndex[actor.id] = actor
        }
    }

    private fun insertRoles(vararg newRoles: Role) {
        newRoles.forEach { role ->
            rolesByActorIdIndex.getOrCreate(role.actorId).add(role)
            rolesByMovieIdIndex.getOrCreate(role.movieId).add(role)
        }
    }
    private fun <K, V> MutableMap<K, MutableList<V>>.getOrCreate(key: K) =
        this[key] ?: mutableListOf<V>().apply {
            this@getOrCreate[key] = this
        }

    @GET
    @Path("reset")
    @Produces(MediaType.TEXT_PLAIN)
    fun resetDatabase(): Response {
        moviesByIdIndex.clear()
        actorsByIdIndex.clear()
        rolesByActorIdIndex.clear()
        rolesByMovieIdIndex.clear()

        insertMovies(
            Movie("m1", "The Transporter", "Jason Statham kicks a guy in the face", "r3"),
            Movie("m2", "Transporter 2", "Jason Statham kicks a bunch of guys in the face", "r4"),
            Movie("m3", "Hobbs and Shaw", "Cars, Explosions and Stuff", "r3"),
            Movie("m4", "Jumanji", "The Rock smolders", "r3"),
        )

        insertActors(
            Actor("a1", "Jason Statham"),
            Actor("a2", "The Rock"),
            Actor("a3", "Shu Qi"),
            Actor("a4", "Amber Valletta"),
            Actor("a5", "Kevin Hart"),
        )
        insertRoles(
            Role("m1", "a1", "Frank Martin", 1),
            Role("m1", "a3", "Lai", 2),
            Role("m2", "a1", "Frank Martin", 1),
            Role("m2", "a4", "Audrey Billings", 2),
            Role("m3", "a2", "Hobbs", 1),
            Role("m3", "a1", "Shaw", 2),
            Role("m4", "a2", "Spencer", 1),
            Role("m4", "a5", "Fridge", 2),
        )
        return ok(1)
    }


    companion object {
        private val NotRated = Rating(id = "r0", name = "Not Rated", description = "Not yet rated")
        private val G = Rating(id = "r1", name = "G", description = "General Audiences")
        private val PG = Rating(id = "r2", name = "PG", description = "Parental Guidance Suggested")
        private val PG13 = Rating(id = "r3", name = "PG-13", description = "Unsuitable for those under 13")
        private val R = Rating(id = "r4", name = "R", description = "Restricted - 17 and older")
        private val ratings = listOf(NotRated, G, PG, PG13, R)
        private val ratingsIndex = ratings.associateBy { it.id }
    }
}