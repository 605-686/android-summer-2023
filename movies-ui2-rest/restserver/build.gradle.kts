plugins {
    application
    id("org.jetbrains.kotlin.jvm")
}

kotlin {
    jvmToolchain(17)
}

dependencies {
    implementation("org.glassfish.jersey.containers:jersey-container-grizzly2-http:3.0.2")
    implementation("org.glassfish.jersey.media:jersey-media-json-jackson:3.0.2")
    implementation("jakarta.activation:jakarta.activation-api:2.0.1")
    implementation("org.glassfish.jersey.containers:jersey-container-jetty-http:3.0.2")
    implementation("org.glassfish.jersey.core:jersey-server:3.0.2")
    implementation("org.glassfish.jersey.containers:jersey-container-servlet-core:3.0.2")
    implementation("org.glassfish.jersey.inject:jersey-hk2:3.0.2")
}

application {
    mainClass.set("com.androidbyexample.movies.restserver.RunServerKt")
}