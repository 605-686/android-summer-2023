package com.androidbyexample.movies.screens

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.androidbyexample.movies.ActorDisplayScreen
import com.androidbyexample.movies.ActorListScreen
import com.androidbyexample.movies.MovieDisplayScreen
import com.androidbyexample.movies.MovieEditScreen
import com.androidbyexample.movies.MovieListScreen
import com.androidbyexample.movies.MovieViewModel
import com.androidbyexample.movies.RatingDisplayScreen
import com.androidbyexample.movies.RatingListScreen

@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit,
) {
    BackHandler {
        viewModel.popScreen()
    }

    val ratings by viewModel.ratingsFlow.collectAsState(initial = emptyList())
    val movies by viewModel.moviesFlow.collectAsState(initial = emptyList())
    val actors by viewModel.actorsFlow.collectAsState(initial = emptyList())

    when(val screen = viewModel.screen) {
        null -> onExit()

        RatingListScreen -> RatingList(
            ratings = ratings,
            onSelectListScreen = viewModel::setScreenStack,
            onResetDatabase = viewModel::resetDatabase,
            onRatingClick = { id ->
                viewModel.pushScreen(RatingDisplayScreen(id))
            },
            selectedItemIds = viewModel.selectedItemIds,
            onClearSelections = viewModel::clearSelections,
            onToggleSelection = viewModel::toggleSelection,
            onDeleteSelectedItems = viewModel::deleteSelectedRatings,
        )
        MovieListScreen -> MovieList(
            movies = movies,
            onSelectListScreen = viewModel::setScreenStack,
            onResetDatabase = viewModel::resetDatabase,
            onMovieClick = { id ->
                viewModel.pushScreen(MovieDisplayScreen(id))
            },
            selectedItemIds = viewModel.selectedItemIds,
            onClearSelections = viewModel::clearSelections,
            onToggleSelection = viewModel::toggleSelection,
            onDeleteSelectedItems = viewModel::deleteSelectedMovies,
        )
        ActorListScreen -> ActorList(
            actors = actors,
            onSelectListScreen = viewModel::setScreenStack,
            onResetDatabase = viewModel::resetDatabase,
            onActorClick = { id ->
                viewModel.pushScreen(ActorDisplayScreen(id))
            },
            selectedItemIds = viewModel.selectedItemIds,
            onClearSelections = viewModel::clearSelections,
            onToggleSelection = viewModel::toggleSelection,
            onDeleteSelectedItems = viewModel::deleteSelectedActors,
        )

        is RatingDisplayScreen -> RatingDisplay(
            ratingId = screen.id,
            fetchRatingWithMovies = { id ->
                viewModel.getRatingWithMovies(id)
            },
            onSelectListScreen = viewModel::setScreenStack,
            onResetDatabase = viewModel::resetDatabase,
            onMovieClick = { id ->
                viewModel.pushScreen(MovieDisplayScreen(id))
            }
        )
        is MovieDisplayScreen -> {
// BEGIN ADD FOR UPDATE
            val movieWithCastDto by
                viewModel
                    .getMovieWithCastFlow(screen.id)
                    .collectAsState(initial = null)
// END ADD FOR UPDATE
            MovieDisplay(
                movieWithCastDto = movieWithCastDto, // ADD FOR UPDATE
// BEGIN REMOVE FOR UPDATE
//                movieId = screen.id,
//                fetchMovieWithCast = viewModel::getMovieWithCast,
// END REMOVE FOR UPDATE
                onSelectListScreen = viewModel::setScreenStack,
                onResetDatabase = viewModel::resetDatabase,
                onActorClick = { id ->
                    viewModel.pushScreen(ActorDisplayScreen(id))
                },
                onEdit = { id ->
                    viewModel.pushScreen(MovieEditScreen(id))
                }
            )
        }
        is MovieEditScreen -> MovieEdit(
            movieId = screen.id,
            fetchMovie = viewModel::getMovie,
            onSelectListScreen = viewModel::setScreenStack,
            onResetDatabase = viewModel::resetDatabase,
            onMovieUpdate = viewModel::updateMovie,
        )
        is ActorDisplayScreen -> ActorDisplay(
            actorId = screen.id,
            fetchActorWithFilmography = viewModel::getActorWithFilmography,
            onSelectListScreen = viewModel::setScreenStack,
            onResetDatabase = viewModel::resetDatabase,
            onMovieClick = { id ->
                viewModel.pushScreen(MovieDisplayScreen(id))
            }
        )
    }
}