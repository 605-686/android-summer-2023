package com.androidbyexample.movies.components

import androidx.annotation.StringRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movies.Screen
import com.androidbyexample.movies.repository.HasId

@Composable
fun <T: HasId> ListScaffold(
    title: String,
    onSelectListScreen: (Screen) -> Unit,
    onResetDatabase: () -> Unit,
    items: List<T>,
    icon: ImageVector,
    @StringRes iconDescriptionId: Int,
    onItemClick: (String) -> Unit,

    // for selection
    selectedItemIds: Set<String>,
    onClearSelections: () -> Unit,
    onToggleSelection: (String) -> Unit,
    onDeleteSelectedItems: () -> Unit,

    itemContent: @Composable (T) -> Unit,
) = MovieScaffold(
    title = title,
    onSelectListScreen = onSelectListScreen,
    selectedItemCount = selectedItemIds.size,
    onResetDatabase = onResetDatabase,
    onDeleteSelectedItems = onDeleteSelectedItems,
    onClearSelections = onClearSelections,
) { paddingValues ->
    LazyColumn(
        modifier = Modifier.padding(paddingValues)
    ) {
        items(
            items = items,
            key = { it.id },
        ) { item ->

            val containerColor =
                if (item.id in selectedItemIds) {
                    MaterialTheme.colorScheme.primaryContainer
                } else {
                    MaterialTheme.colorScheme.surfaceVariant
                }

            // The .pointerInput modifier captures any values referenced - it would never
            //   know the selections have changed. By using rememberUpdatedState, we create
            //   a bucket in the tree that can be updated on recomposition and force
            //   the pointerInput lambda to always get the latest value
            // (Fortunately you don't need to do this often, but if a lambda doesn't seem to
            //   see changes in parameters, you may need to do this.)
            val selectedIds by rememberUpdatedState(newValue = selectedItemIds)

            Card(
                elevation = CardDefaults.cardElevation(),
                colors = CardDefaults.cardColors(
                    containerColor = containerColor,
                ),
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()

                    // NOTE: pointerInput captures the value of selectedItemIds
                    //       By referencing it through a rememberUpdatedState above, it'll see
                    //       updates. Note that we _must_ reference selectedIds inside the lambda
                    //       to see the updated value.
                    //       Note - we could also pass selectedItemIds instead of true - this would
                    //              force pointerInput to reinitialize whenever the selection changes
                    //              That might be expensive to recreate the gesture handler every
                    //              time, OR if the gesture handler held state that would be cleared
                    //              might not give the proper results.
                    .pointerInput(true) {
                        detectTapGestures(
                            onLongPress = {
                                onToggleSelection(item.id)
                            },
                            onTap = {
                                if (selectedIds.isNotEmpty()) {
                                    onToggleSelection(item.id)
                                } else {
                                    onItemClick(item.id)
                                }
                            }
                        )
                    },
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .padding(8.dp)
                        .clickable { onItemClick(item.id) }
                ) {
                    IconButton(
                        onClick = { onToggleSelection(item.id) },
                        modifier = Modifier
                            .size(48.dp)
                            .clip(CircleShape),
                        colors = IconButtonDefaults.filledIconButtonColors()
                    ) {
                        Icon(
                            imageVector = icon,
                            contentDescription = stringResource(id = iconDescriptionId),
                            modifier = Modifier.padding(4.dp)
                        )
                    }
                    itemContent(item)
                }
            }
        }
    }
}