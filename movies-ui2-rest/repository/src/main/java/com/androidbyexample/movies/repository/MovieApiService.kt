package com.androidbyexample.movies.repository

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

const val BASE_URL = "http://10.0.2.2:8080" // host computer for emulator
// NOTE: In a real app, you might allow the user to set this via settings

interface MovieApiService {
    @GET("movie")
    suspend fun getMovies(): Response<List<MovieDto>>

    @GET("actor")
    suspend fun getActors(): Response<List<ActorDto>>

    @GET("rating")
    suspend fun getRatings(): Response<List<RatingDto>>

    @GET("rating/{id}/movies")
    suspend fun getRatingWithMovies(@Path("id") id: String): RatingWithMoviesDto

    @GET("movie/{id}/roles")
    suspend fun getMovieWithRoles(@Path("id") id: String): MovieWithCastDto

    @GET("actor/{id}/roles")
    suspend fun getActorWithRoles(@Path("id") id: String): ActorWithFilmographyDto

    @GET("reset")
    suspend fun reset(): Response<Int>

    companion object {
        fun create() =
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(MovieApiService::class.java)
    }
}