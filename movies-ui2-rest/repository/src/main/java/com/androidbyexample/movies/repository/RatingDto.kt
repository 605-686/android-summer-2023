package com.androidbyexample.movies.repository

import com.androidbyexample.movies.data.RatingEntity
import com.androidbyexample.movies.data.RatingWithMovies

data class RatingDto(
    override val id: String,
    val name: String,
    val description: String,
): HasId

internal fun RatingEntity.toDto() =
    RatingDto(id = id, name = name, description = description)
internal fun RatingDto.toEntity() =
    RatingEntity(id = id, name = name, description = description)

data class RatingWithMoviesDto(
    val rating: RatingDto,
    val movies: List<MovieDto>,
)

// only need the toDto(); we don't use this to do database updates
internal fun RatingWithMovies.toDto() =
    RatingWithMoviesDto(
        rating = rating.toDto(),
        movies = movies.map { it.toDto() },
    )
