package com.androidbyexample.movies.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class MovieRestRepository(
    val coroutineScope: CoroutineScope,
): MovieRepository {
    inner class ListFlowManager<T>(
        private val fetcher: suspend () -> Response<List<T>>
    ) {
        private val _flow = MutableStateFlow(emptyList<T>())
        init {
            fetch()
        }
        val flow: Flow<List<T>>
            get() = _flow

        fun fetch() =
            coroutineScope.launch(Dispatchers.IO) {
                _flow.value = fetcher().takeIf { it.isSuccessful }?.body() ?: emptyList()
            }
    }

    private val movieApiService = MovieApiService.create()

    private val moviesFlowManager = ListFlowManager { movieApiService.getMovies() }
    private val actorsFlowManager = ListFlowManager { movieApiService.getActors() }
    private val ratingsFlowManager = ListFlowManager { movieApiService.getRatings() }

    override val ratingsFlow: Flow<List<RatingDto>> = ratingsFlowManager.flow
    override val moviesFlow: Flow<List<MovieDto>> = moviesFlowManager.flow
    override val actorsFlow: Flow<List<ActorDto>> = actorsFlowManager.flow

    override suspend fun getRatingWithMovies(id: String): RatingWithMoviesDto = withContext(Dispatchers.IO) {
        movieApiService.getRatingWithMovies(id)
    }

    override suspend fun getMovieWithCast(id: String): MovieWithCastDto = withContext(Dispatchers.IO) {
        movieApiService.getMovieWithRoles(id)
    }

    override suspend fun getActorWithFilmography(id: String): ActorWithFilmographyDto = withContext(Dispatchers.IO) {
        movieApiService.getActorWithRoles(id)
    }

    override fun getMovieWithCastFlow(id: String): Flow<MovieWithCastDto> {
        TODO("Not yet implemented")
    }

    override suspend fun getMovie(id: String): MovieDto {
        TODO("Not yet implemented")
    }

    override suspend fun insert(movie: MovieDto) {
        TODO("Not yet implemented")
    }

    override suspend fun insert(actor: ActorDto) {
        TODO("Not yet implemented")
    }

    override suspend fun insert(rating: RatingDto) {
        TODO("Not yet implemented")
    }

    override suspend fun update(movie: MovieDto) {
        TODO("Not yet implemented")
    }

    override suspend fun update(actor: ActorDto) {
        TODO("Not yet implemented")
    }

    override suspend fun update(rating: RatingDto) {
        TODO("Not yet implemented")
    }

    override suspend fun delete(movie: MovieDto) {
        TODO("Not yet implemented")
    }

    override suspend fun delete(actor: ActorDto) {
        TODO("Not yet implemented")
    }

    override suspend fun delete(rating: RatingDto) {
        TODO("Not yet implemented")
    }

    override suspend fun deleteMoviesById(ids: Set<String>) {
        TODO("Not yet implemented")
    }

    override suspend fun deleteActorsById(ids: Set<String>) {
        TODO("Not yet implemented")
    }

    override suspend fun deleteRatingsById(ids: Set<String>) {
        TODO("Not yet implemented")
    }

    override suspend fun resetDatabase() {
        movieApiService.reset()
        actorsFlowManager.fetch()
        moviesFlowManager.fetch()
        ratingsFlowManager.fetch()
    }
}