@file:OptIn(ExperimentalPermissionsApi::class)

package com.javadude.service

import android.Manifest
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.permissions.shouldShowRationale
import com.javadude.service.ui.theme.ServiceTheme

class MusicActivity : ComponentActivity() {
    private var musicState by mutableStateOf<State>(Stopped)

    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ServiceTheme {
                Surface(color = MaterialTheme.colorScheme.background) {
                    val showUi = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                        val notificationPermissionState =
                            rememberPermissionState(Manifest.permission.POST_NOTIFICATIONS)

                        if (notificationPermissionState.status.isGranted) {
                            true
                        } else {
                            Column {
                                val textToShow =
                                    if (notificationPermissionState.status.shouldShowRationale) {
                                        "Notifications won't be shown"
                                    } else {
                                        "Notifications for a music player app are quite useful! Please grant the permission."
                                    }
                                Text(textToShow)
                                Button(onClick = { notificationPermissionState.launchPermissionRequest() }) {
                                    Text("Request permission")
                                }
                            }
                            false
                        }
                    } else {
                        true
                    }

                    if (showUi) {
                        UI(musicState) {
                            musicState = it
                            when (it) {
                                Paused -> startService(PAUSE)
                                Playing -> startService(PLAY)
                                Stopped -> startService(STOP)
                            }
                        }
                    }
                }
            }
        }

        // listen for play/pause/stop broadcasts from the notification's action buttons
        registerReceiver(broadcastReceiver, receiverIntentFilter, RECEIVER_NOT_EXPORTED)
    }

    // sends a "start" intent to the service (whether it's started or not)
    // this goes to onStartCommand in the service, passing the action to perform (play/pause/stop)
    private fun startService(actionCode: Int) {
        ContextCompat.startForegroundService(this,
            Intent(this, RemoteMusicService::class.java)
                .putExtra(EXTRA_ACTION, actionCode)
                .putExtra(EXTRA_SONG, R.raw.alexander_nakarada_one_bard_band)
                .putExtra(EXTRA_TITLE, "One Bard Band")
        )
    }

    // broadcast receiver that listens for the broadcast intents sent by
    //   action buttons on the notification
    private val broadcastReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                ACTION_PLAY -> musicState = Playing
                ACTION_PAUSE -> musicState = Paused
                ACTION_STOP -> musicState = Stopped
                // otherwise ignore
            }
        }
    }

    override fun onDestroy() {
        unregisterReceiver(broadcastReceiver)
        super.onDestroy()
    }

}

@Composable
fun StateButton(@StringRes textId: Int, newState: State, onStateChange: (State) -> Unit) =
    Button(onClick = { onStateChange(newState) }, modifier = Modifier.padding(8.dp)) {
        Text(text = stringResource(id = textId))
    }

@Composable
fun UI(
    musicState: State,
    onStateChange: (State) -> Unit
) {
    Column(modifier = Modifier.fillMaxSize()) {
        Text(
            text = """
                One Bard Band by Alexander Nakarada | https://www.serpentsoundstudios.com
                
                Music promoted by https://www.free-stock-music.com
                
                Attribution 4.0 International (CC BY 4.0)
                https://creativecommons.org/licenses/by/4.0/
            """.trimIndent(),
            modifier = Modifier.padding(8.dp)
        )
        Row(modifier = Modifier.fillMaxWidth()) {
            if (musicState == Playing) {
                StateButton(textId = R.string.pause, newState = Paused, onStateChange = onStateChange)
            } else {
                StateButton(textId = R.string.play, newState = Playing, onStateChange = onStateChange)
            }
            if (musicState != Stopped) {
                StateButton(textId = R.string.stop, newState = Stopped, onStateChange = onStateChange)
            }
        }
    }
}
