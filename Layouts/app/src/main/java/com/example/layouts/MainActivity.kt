@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.layouts

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstrainScope
import androidx.constraintlayout.compose.ConstrainedLayoutReference
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintLayoutScope
import androidx.constraintlayout.compose.Dimension
import com.example.layouts.ui.theme.LayoutsTheme
import kotlin.math.max

// for fun, see https://www.youtube.com/watch?v=xcfEQO0k_gU
//    Google talk on layouts and graphics

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LayoutsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
//                    FormUsingRowsInsideAColumn()
//                    FormUsingColumnsInsideARow()
//                    FormUsingConstraintLayout()
//                    FormUsingConstraintLayout2()
//                    FormUsingConstraintLayout3()
//                    FormUsingFormLayout()
                    SampleBorderLayout()
                }
            }
        }
    }
}

@Composable
fun Label(
    text: String,
    modifier: Modifier = Modifier,
) {
    Text(
        text = text,
        style = MaterialTheme.typography.labelLarge,
        modifier = modifier.padding(8.dp),
    )
}

@Composable
fun RowScope.RowEdit(
    text: String,
    modifier: Modifier = Modifier,
) {
    OutlinedTextField(
        value = text,
        textStyle = MaterialTheme.typography.titleMedium,
        onValueChange = {}, // only doing a layout example
        modifier = modifier
            .padding(8.dp)
            .weight(1f)
    )
}

@Composable
fun Edit(
    text: String,
    modifier: Modifier = Modifier,
) {
    OutlinedTextField(
        value = text,
        textStyle = MaterialTheme.typography.titleMedium,
        onValueChange = {}, // only doing a layout example
        modifier = modifier
            .padding(8.dp)
    )
}


@Composable
fun FormUsingRowsInsideAColumn() {
    Column {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label(text = "First Name")
            RowEdit("Scott")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label(text = "Last Name")
            RowEdit("Stanchfield")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label(text = "Age")
            RowEdit("56")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label(text = "Street")
            RowEdit("123 Sesame")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label(text = "City")
            RowEdit("New York")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label(text = "State")
            RowEdit("NY")
        }
        Row(verticalAlignment = Alignment.CenterVertically) {
            Label(text = "Zip Code")
            RowEdit("10101")
        }
    }
}

@Composable
fun FormUsingColumnsInsideARow() {
    Row {
        Column(modifier = Modifier.fillMaxHeight()) {
            Label(text = "First Name")
            Label(text = "Last Name")
            Label(text = "Age")
            Label(text = "Street")
            Label(text = "City")
            Label(text = "State")
            Label(text = "Zip Code")
        }
        Column(modifier = Modifier.fillMaxHeight()) {
            Edit("Scott")
            Edit("Stanchfield")
            Edit("56")
            Edit("123 Sesame")
            Edit("New York")
            Edit("NY")
            Edit("10101")
        }
    }
}

@Composable
fun FormUsingConstraintLayout() {
    ConstraintLayout(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
//            .wrapContentHeight()
            .verticalScroll(rememberScrollState())
    ) {
        val (l1, l2, l3, l4, l5, l6, l7, e1, e2, e3, e4, e5, e6, e7) = createRefs()

        val barrier = createEndBarrier(l1, l2, l3, l4, l5, l6, l7)

        // TODO try chain later for text fields

        ConstrainedLabel(l1, "First Name") {
            start.linkTo(parent.start)
            top.linkTo(e1.top)
            bottom.linkTo(e1.bottom)
        }
        ConstrainedLabel(l2, "Last Name") {
            start.linkTo(parent.start)
            top.linkTo(e2.top)
            bottom.linkTo(e2.bottom)
        }
        ConstrainedLabel(l3, "Age") {
            start.linkTo(parent.start)
            top.linkTo(e3.top)
            bottom.linkTo(e3.bottom)
        }
        ConstrainedLabel(l4, "Street") {
            start.linkTo(parent.start)
            top.linkTo(e4.top)
            bottom.linkTo(e4.bottom)
        }
        ConstrainedLabel(l5, "City") {
            start.linkTo(parent.start)
            top.linkTo(e5.top)
            bottom.linkTo(e5.bottom)
        }
        ConstrainedLabel(l6, "State") {
            start.linkTo(parent.start)
            top.linkTo(e6.top)
            bottom.linkTo(e6.bottom)
        }
        ConstrainedLabel(l7, "Zip Code") {
            start.linkTo(parent.start)
            top.linkTo(e7.top)
            bottom.linkTo(e7.bottom)
        }

        // equivalent to
        //        Text(
        //            text = "First Name",
        //            style = MaterialTheme.typography.labelLarge,
        //            modifier = Modifier
        //                .padding(8.dp)
        //                .constrainAs(l1) {
        //                    start.linkTo(parent.start)
        //                    top.linkTo(e1.top)
        //                    bottom.linkTo(e1.bottom)
        //                }
        //        )

        ConstrainedEdit(e1, "Scott") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(parent.top)
        }
        ConstrainedEdit(e2, "Stanchfield") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e1.bottom)
        }
        ConstrainedEdit(e3, "56") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e2.bottom)
        }
        ConstrainedEdit(e4, "123 Sesame") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e3.bottom)
        }
        ConstrainedEdit(e5, "New York") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e4.bottom)
        }
        ConstrainedEdit(e6, "NY") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e5.bottom)
        }
        ConstrainedEdit(e7, "10101") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(e6.bottom)
        }
    }
}

@Composable
fun FormUsingConstraintLayout2() {
    ConstraintLayout(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxSize()
            .wrapContentHeight()
            .verticalScroll(rememberScrollState())
    ) {
        val (l1, l2, l3, l4, l5, l6, l7, e1, e2, e3, e4, e5, e6, e7) = createRefs()

        val barrier = createEndBarrier(l1, l2, l3, l4, l5, l6, l7)

        createVerticalChain(e1, e2, e3, e4, e5, e6, e7, chainStyle = ChainStyle.Spread)
        // TODO try chain later for text fields

        ConstrainedLabel(l1, "First Name") {
            start.linkTo(parent.start)
            top.linkTo(e1.top)
            bottom.linkTo(e1.bottom)
        }
        ConstrainedLabel(l2, "Last Name") {
            start.linkTo(parent.start)
            top.linkTo(e2.top)
            bottom.linkTo(e2.bottom)
        }
        ConstrainedLabel(l3, "Age") {
            start.linkTo(parent.start)
            top.linkTo(e3.top)
            bottom.linkTo(e3.bottom)
        }
        ConstrainedLabel(l4, "Street") {
            start.linkTo(parent.start)
            top.linkTo(e4.top)
            bottom.linkTo(e4.bottom)
        }
        ConstrainedLabel(l5, "City") {
            start.linkTo(parent.start)
            top.linkTo(e5.top)
            bottom.linkTo(e5.bottom)
        }
        ConstrainedLabel(l6, "State") {
            start.linkTo(parent.start)
            top.linkTo(e6.top)
            bottom.linkTo(e6.bottom)
        }
        ConstrainedLabel(l7, "Zip Code") {
            start.linkTo(parent.start)
            top.linkTo(e7.top)
            bottom.linkTo(e7.bottom)
        }

        // equivalent to
        //        Text(
        //            text = "First Name",
        //            style = MaterialTheme.typography.labelLarge,
        //            modifier = Modifier
        //                .padding(8.dp)
        //                .constrainAs(l1) {
        //                    start.linkTo(parent.start)
        //                    top.linkTo(e1.top)
        //                    bottom.linkTo(e1.bottom)
        //                }
        //        )

        ConstrainedEdit(e1, "Scott") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            top.linkTo(parent.top)
        }
        ConstrainedEdit(e2, "Stanchfield") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedEdit(e3, "56") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedEdit(e4, "123 Sesame") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedEdit(e5, "New York") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedEdit(e6, "NY") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedEdit(e7, "10101") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
    }
}

@Composable
fun FormUsingConstraintLayout3() {
    ConstraintLayout(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxSize()
            .wrapContentHeight()
            .verticalScroll(rememberScrollState())
    ) {
        val labels = listOf(
            "First Name",
            "Last Name",
            "Age",
            "Street",
            "City",
            "State",
            "Zip",
        )
        val values = listOf(
            "Scott",
            "Stanchfield",
            "54",
            "123 Sesame",
            "New York",
            "NY",
            "10101",
        )

        check(labels.size == values.size) { "Must have same number of labels and values" }

        val labelRefs = Array(labels.size) { createRef() }
        val editRefs = Array(values.size) { createRef() }

        val barrier = createEndBarrier(*labelRefs)

        createVerticalChain(*editRefs, chainStyle = ChainStyle.Spread)

        labels.forEachIndexed { n, label ->
            val labelRef = labelRefs[n]
            val editRef = editRefs[n]
            val value = values[n]

            ConstrainedLabel(labelRef, label) {
                start.linkTo(parent.start)
                top.linkTo(editRef.top)
                bottom.linkTo(editRef.bottom)
            }
            ConstrainedEdit(editRef, value) {
                start.linkTo(barrier)
                end.linkTo(parent.end)
            }
        }
    }
}

@Composable
fun ConstraintLayoutScope.ConstrainedLabel(
    ref: ConstrainedLayoutReference,
    text: String,
    constraintBlock: ConstrainScope.() -> Unit,
) {
    Text(
        text = text,
        style = MaterialTheme.typography.labelLarge,
        modifier = Modifier
            .padding(8.dp)
            .constrainAs(ref, constraintBlock)
    )
}

@Composable
fun ConstraintLayoutScope.ConstrainedEdit(
    ref: ConstrainedLayoutReference,
    text: String,
    constraintBlock: ConstrainScope.() -> Unit,
) {
    OutlinedTextField(
        value = text,
        onValueChange = {},
        textStyle = MaterialTheme.typography.titleMedium,
        modifier = Modifier
            .padding(8.dp)
            .constrainAs(ref) {
                constraintBlock()
                width = Dimension.fillToConstraints
            }
    )
}

@Composable
fun FormLayout(
    minWidthForSideBySide: Dp,
    maxLabelWidth: Dp,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit,
) {
    with(LocalDensity.current) {
        val maxLabelWidthPx = maxLabelWidth.toPx()
        val minWidthForSideBySidePx = minWidthForSideBySide.toPx()

        Layout(
            content = content,
            modifier = modifier.verticalScroll(rememberScrollState())
        ) { measurables, constraints ->
            // measure all measurables to get placeables
            // place them

            check(measurables.size % 2 == 0) { "FormLayout requires an even number of @Composables to layout"}

            if (constraints.maxWidth >= minWidthForSideBySidePx) {
                // side by side layout

                val labels = measurables.filterIndexed { n, _ -> n % 2 == 0 }
                val controls = measurables.filterIndexed { n, _ -> n % 2 == 1 }

                val labelConstraints =
                    Constraints(
                        minWidth = 0,
                        minHeight = 0,
                        maxWidth = maxLabelWidthPx.toInt(),
                        maxHeight = Constraints.Infinity,
                    )

                val labelPlaceables = labels.map {
                    it.measure(labelConstraints)
                }

                val labelsWidth = labelPlaceables.maxOf { it.width }
                val controlsWidth = constraints.maxWidth - labelsWidth

                val controlConstraints =
                    Constraints(
                        minWidth = controlsWidth,
                        minHeight = 0,
                        maxWidth = controlsWidth,
                        maxHeight = Constraints.Infinity,
                    )

                val controlPlaceables = controls.map {
                    it.measure(controlConstraints)
                }

                val height = max(
                    labelPlaceables.sumOf { it.height },
                    controlPlaceables.sumOf { it.height },
                )

                layout(constraints.maxWidth, height) {
                    var y = 0
                    labelPlaceables.forEachIndexed { n, label ->
                        val control = controlPlaceables[n]

                        val rowHeight = max(label.height, control.height)
                        val labelOffsetY = (rowHeight - label.height) / 2
                        val controlOffsetY = (rowHeight - control.height) / 2
                        label.placeRelative(0, y + labelOffsetY)
                        control.placeRelative(labelsWidth, y + controlOffsetY)
                        y += rowHeight
                    }
                }

            } else {
                // single-column layout
                val newConstraints =
                    Constraints(
                        minWidth = constraints.minWidth,
                        maxWidth = constraints.maxWidth,
                        minHeight = 0,
                        maxHeight = Constraints.Infinity,
                    )

                val placeables = measurables.map {
                    it.measure(newConstraints)
                }

                val height = placeables.sumOf { it.height }

                layout(constraints.maxWidth, height) {
                    var y = 0
                    placeables.forEach {
                        it.placeRelative(0, y)
                        y += it.height
                    }
                }
            }
        }
    }
}


@Composable
fun FormUsingFormLayout() {
    FormLayout(maxLabelWidth = 200.dp, minWidthForSideBySide = 700.dp) {
        Label("First Name")
        Edit("Scott")
        Label("Last Name")
        Edit("Stanchfield")
        Label("Age")
        Edit("54")
        Label("Street")
        Edit("123 Sesame")
        Label("City")
        Edit("New York")
        Label("State")
        Edit("NY")
        Label("Zip")
        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
//        Label("Zip")
//        Edit("10101")
    }
}

@Composable
fun SampleBorderLayout() {
    BorderLayout(
        modifier = Modifier.fillMaxSize(),
        north = { Text("NORTH", modifier = it.background(Color.Blue))},
        south = { Text("SOUTH", modifier = it.background(Color.Blue))},
        east = { Text("EAST", modifier = it.background(Color.Green))},
        west = { Text("WEST", modifier = it.background(Color.Green))},
        center = { Text("CENTER", modifier = it.background(Color.Red))},
    )
}

@Composable
fun BorderLayout(
    north: @Composable ((Modifier) -> Unit)? = null,
    south: @Composable ((Modifier) -> Unit)? = null,
    east: @Composable ((Modifier) -> Unit)? = null,
    west: @Composable ((Modifier) -> Unit)? = null,
    center: @Composable ((Modifier) -> Unit)? = null,
    modifier: Modifier = Modifier,
) {
    Column(modifier = modifier) {
        north?.let {
            Box(
               modifier = Modifier
                   .fillMaxWidth()
                   .wrapContentHeight()
            ) {
                it(Modifier.fillMaxWidth())
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        ) {
            west?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .wrapContentWidth()
                ) {
                    it(Modifier.fillMaxHeight())
                }
            }
            center?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(1f)
                ) {
                    it(Modifier.fillMaxSize())
                }
            }
            east?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .wrapContentWidth()
                ) {
                    it(Modifier.fillMaxHeight())
                }
            }
        }
        south?.let {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            ) {
                it(Modifier.fillMaxWidth())
            }
        }
    }
}
