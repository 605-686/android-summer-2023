# Android Summer 2023

## Videos

   * Week 1: https://youtu.be/Do1ExAOvKEg
   * Week 2: https://youtu.be/JyDATaT9Hyw
   * Week 3: https://youtu.be/xMFUJCRO4m8
   * Week 4: Instructor Sick - please see the following sections on https://androidbyexample.com. Example updated to latest Compose and Android/Gradle/dependency versions.
     * [4.1 Compose Basics](https://androidbyexample.com/week-04/4.1-Compose-basics/index.html)
     * [4.2 Movie UI Part 1](https://androidbyexample.com/week-04/4.2-Movie-Ui-1/index.html)
   * Week 5: https://youtu.be/StQ6JVjqpQ4
   * Week 6: Instructor Sick - please see the following sections on https://androidbyexample.com. Examples updated to latest Compose and Android/Gradle/dependency versions
     * [6.1 Compose Graphics Part 1](https://androidbyexample.com/week-06/6.1-Compose-graphics-1)
   * Week 7: https://youtu.be/XHDQ3hF4878
   * Week 8: https://youtu.be/GooP76v8tvE
   * Week 9: Instructor networking problems- please see the following sections on https://androidbyexample.com. Examples updated to latest Compose and Android/Gradle/dependency versions
     * [9.1 Google Maps](https://androidbyexample.com/week-09/9.1-Google-map/index.html)
     * [10.2 Services](https://androidbyexample.com/week-10/10.2-Services/index.html)
   * Week 10: https://youtu.be/9Kavv1zros0
   * Week 11: https://youtu.be/yNSiK2URki4
   * Week 12: https://youtu.be/z73kLejdbWw
   
## New New Project Setup (AKA Old Project Setup)

I decided to not use the custom new-project wizard this term. Please follow the 
project setup described in week 3.
